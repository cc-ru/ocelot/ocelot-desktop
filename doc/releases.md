# How to make a release
A step-by-step guide for Ocelot Desktop maintainers.

Let's assume you want to release a new version, say, `1.33.7`.

1. Switch to the `develop` branch.
2. If ocelot-brain was changed since the last release, update its version first.
   Update the submodule to include this change.
3. Update the version of Ocelot Desktop in `build.sbt`; commit the changes.
   The commit message should just say "Version 1.33.7".
4. Switch to `master` and merge `develop` into it.
5. Create an annotated tag: `git -a v1.33.7`.
   On the first line, write "Version 1.33.7", followed by a blank line.
   Then describe the changes in Markdown.
   Make sure not to use `#` in the text, since git will treat it as a comment.
   See other releases for examples.
6. Switch back to `develop` and merge `master` back into it.
7. Push everything to GitLab: `git push origin master develop v1.33.7`.
8. Wait until all the pipelines succeed.
   The release job will process the newly added tag and create a release for it.

Easy as 1-2-3-4-5-6-7-8.
