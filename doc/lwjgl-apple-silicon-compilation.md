# How to compile LWJGL2 for Apple Silicon
LWJGL2 does not provide official native ARM support, therefore Ocelot uses a specially modified version by **shadowfacts**

Article: https://shadowfacts.net/2022/lwjgl-arm64/

Repository: https://github.com/shadowfacts/lwjgl2-arm64

This procedure is completely optional, as the precompiled library is already checked into the repository at `lib/native/liblwjgl-arm64.dylib`

## Compilation
This assumes that you are running macOS on **Apple Silicon** *(cross-compiling LWJGL seems to be impossible)*

1. Acquire a JDK8 built for ARM - for example, [Zulu 8 JDK](https://www.azul.com/downloads/?version=java-8-lts&os=macos&architecture=arm-64-bit&package=jdk#zulu), and add it to your `JAVA_HOME`
2. Get [`maven`](https://maven.apache.org/) and [`ant`](https://ant.apache.org/) - you may install them via `brew`, but be careful, as they will try to install a JDK as a dependency
3. Clone the LWJGL repository: 
```bash
% git clone https://github.com/shadowfacts/lwjgl-arm64.git
```
4. Run the following commands in the repo:
```bash
% ant generate-all
% ant jars
% ant compile-native
```
5. Copy the compiled library from `libs/macosx/liblwjgl.dylib` into the `ocelot-desktop` project as `lib/native/liblwjgl-arm64.dylib`