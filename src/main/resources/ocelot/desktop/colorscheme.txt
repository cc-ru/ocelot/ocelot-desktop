Accent = #bd353e

AboutLogo                   = #ffffffee
AboutForeground             = #eeeeee
AboutForegroundSubtle       = #eeeeeeee
AboutButtonForeground       = #222222
AboutButtonBorder           = #444444
AboutButtonBackground       = #777777
AboutButtonBackgroundActive = #888888

PortDown  = #8382d8
PortUp    = #75bdc1
PortNorth = #c8ca5f
PortSouth = #990da3
PortEast  = #7ec95f
PortWest  = #db7d75
PortAny   = #9b9b9b

Tier0 = #ababab
Tier1 = #ffff66
Tier2 = #66ffff
Tier3 = #c354cd

Label = #333333
LabelError = #aa0000

Scrollbar      = #e5e5e526
ScrollbarThumb = #cc3f72

ConnectionBorder = #191919
ConnectionNew    = #4c7f66
ConnectionDel    = #7f3333
Connection       = #909090

NodeSelectorRing       = #4c7f66
NodeSelectorBackground = #000000bb

ContextMenuBackground = #222222ee
ContextMenuBorder     = #444444
ContextMenuText       = #b0b0b0
ContextMenuHover      = #363636
ContextMenuSeparator  = #444444
ContextMenuIcon       = #7F7F7F

ComputerAddress = #333333

ScreenOff = #000000

StatusBar           = #181818
StatusBarActive     = #ffffff04
StatusBarBorder     = #222222
StatusBarForeground = #777777
StatusBarIcon       = #bbbbbb
StatusBarKey        = #bbbbbb
StatusBarTPS        = #77aa77
StatusBarFPS        = #7777aa

TitleBarBackground       = #181818
TitleBarBorder           = #222222
TitleBarButtonForeground = #bbbbbb
TitleBarButtonActive     = #333333

TextInputBorder              = #888888
TextInputBorderDisabled      = #666666
TextInputBorderFocused       = #336666
TextInputBackground          = #aaaaaa
TextInputBackgroundActive    = #bbbbbb
TextInputForeground          = #333333
TextInputForegroundDisabled  = #888888
TextInputBorderError         = #aa8888
TextInputBorderErrorDisabled = #aa8888
TextInputBorderErrorFocused  = #cc6666

ButtonBackground         = #aaaaaa
ButtonBackgroundActive   = #bbbbbb
ButtonBorder             = #888888
ButtonForeground         = #333333
ButtonForegroundPressed  = #444444
ButtonBackgroundDisabled = #333333
ButtonBorderDisabled     = #666666
ButtonForegroundDisabled = #888888
ButtonConfirm            = #336633

BottomDrawerBorder = #888888

HistogramBarEmpty    = #336633
HistogramBarTop      = #ccfdcc
HistogramBarFill     = #64cc65
HistogramGrid        = #336633
HistogramFill        = #73ff7360
HistogramFillWarning = #ffda7360
HistogramFillError   = #ff737f60
HistogramEdge        = #ccfdcc
HistogramEdgeWarning = #fdeacc
HistogramEdgeError   = #fdccd0

VerticalMenuBackground      = #c6c6c6
VerticalMenuEntryIcon       = #888888
VerticalMenuEntryActive     = #dfdfdf
VerticalMenuEntryForeground = #333333
VerticalMenuBorder          = #dfdfdf

SliderBackground       = #aaaaaa
SliderBackgroundActive = #bbbbbb
SliderBorder           = #888888
SliderTick             = #989898
SliderHandler          = #bbbbbb
SliderForeground       = #333333

CheckboxBackground       = #aaaaaa
CheckboxBackgroundActive = #bbbbbb
CheckboxBorder           = #888888
CheckboxForeground       = #333333
CheckboxLabel            = #333333

NotificationError = #FF204E
NotificationWarning = #ffbf00
NotificationInfo = #20A2FF

Note0  = #77D700
Note1  = #95C000
Note2  = #B2A500
Note3  = #CC8600
Note4  = #E26500
Note5  = #F34100
Note6  = #FC1E00
Note7  = #FE000F
Note8  = #F70033
Note9  = #E8005A
Note10 = #CF0083
Note11 = #AE00A9
Note12 = #8600CC
Note13 = #5B00E7
Note14 = #2D00F9
Note15 = #020AFE
Note16 = #0037F6
Note17 = #0068E0
Note18 = #009ABC
Note19 = #00C68D
Note20 = #00E958
Note21 = #00FC21
Note22 = #1FFC00
Note23 = #59E800
Note24 = #94C100

PlotGrid = #336633
PlotFill = #ccfdcc25
PlotLine = #ccfdcc
PlotText = #333333

SoundCardWaveOff = #33663340
SoundCardWaveActive = #339933

SoundCardWireOff = #929292
SoundCardWire0 = #237f23
SoundCardWire1 = #237f77
SoundCardWire2 = #23567f
SoundCardWire3 = #69237f
SoundCardWire4 = #7f2331
SoundCardWire5 = #7f5123
SoundCardWire6 = #7f7723
SoundCardWire7 = #000000

ErrorMessage = #ff3366
LogParticle = #ff8521

FloppyIconButtonBackgroundSelected = #333333
FloppyIconButtonBorderSelected = #666666

Grid3DColor = #303030
Grid3DPosX = #c9205a
Grid3DNegX = #6e213b
Grid3DPosZ = #255cba
Grid3DNegZ = #264170

LogBackground = #222222
LogBorder = #666666
LogEntryBackground = #cccccc
LogEntryBorder = #888888
LogEntryTxBackground = #185e6d
LogEntryTxForeground = #cccccc
LogEntryTxBorder = #183a41

OcelotCardTooltip = #c1905f

RackRelayButtonText = #e0e0e0

Flash = #ffffff

RelayTextLow  = #009900
RelayTextMid  = #999900
RelayTextHigh = #990000
