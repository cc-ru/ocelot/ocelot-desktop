#version 140

in vec4 fColor;
in vec2 fUV;
in vec4 fTextColor;
in vec2 fTextUV;
out vec4 outColor;

uniform sampler2D uTexture;
uniform sampler2D uTextTexture;

void main() {
    float alpha = texture(uTextTexture, fTextUV).r;
    vec4 color = mix(texture(uTexture, fUV) * fColor, fTextColor, alpha);
    outColor = vec4(color.rgb * color.a, color.a);
}
