#version 140

in vec2 inPos;
in vec2 inUV;

in vec4 inColor;
in vec4 inTextColor;
in vec3 inTransform0;
in vec3 inTransform1;
in vec3 inUVTransform0;
in vec3 inUVTransform1;
in vec3 inTextUVTransform0;
in vec3 inTextUVTransform1;

out vec4 fColor;
out vec2 fUV;
out vec4 fTextColor;
out vec2 fTextUV;

uniform mat3 uProj;

vec4 colorToLinear(vec4 source) {
    bvec3 cutoff = lessThan(source.rgb, vec3(0.04045));
    vec3  higher = pow((source.rgb + vec3(0.055)) / vec3(1.055), vec3(2.4));
    vec3  lower  = source.rgb / vec3(12.92);

    return vec4(mix(higher, lower, cutoff), source.a);
}

void main() {
    fColor = colorToLinear(inColor);
    fTextColor = colorToLinear(inTextColor);

    mat3 modelMatrix = transpose(mat3(inTransform0, inTransform1, vec3(0.0, 0.0, 1.0)));
    vec3 transformed = uProj * modelMatrix * vec3(inPos, 1.0);

    gl_Position = vec4(transformed.xy, 1.0, 1.0);

    mat3 uvMatrix = transpose(mat3(inUVTransform0, inUVTransform1, vec3(0.0, 0.0, 1.0)));
    fUV = (uvMatrix * vec3(inUV, 1.0)).xy;

    uvMatrix = transpose(mat3(inTextUVTransform0, inTextUVTransform1, vec3(0.0, 0.0, 1.0)));
    fTextUV = (uvMatrix * vec3(inUV, 1.0)).xy;
}
