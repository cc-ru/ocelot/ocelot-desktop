#version 140

in vec4 fColor;
in vec3 fNormal;
in vec2 fUV;
in float fLightingFactor;
in float fTextureFactor;

out vec4 outColor;

uniform vec3 uLightDir;
uniform vec3 uLightColor;
uniform vec3 uAmbientLightColor;

uniform sampler2D uTexture;

void main() {
    vec3 normal = normalize(fNormal);

    vec4 texColor = mix(vec4(1), texture(uTexture, fUV), fTextureFactor);
    vec3 albedo = texColor.rgb * fColor.rgb;
    float alpha = texColor.a * fColor.a;

    vec3 light = uAmbientLightColor;
    light += clamp(dot(uLightDir, normal), 0.0, 1.0) * uLightColor;
    light = mix(vec3(1), light, fLightingFactor);

    vec3 color = light * albedo;
    outColor = vec4(color * alpha, alpha);
}
