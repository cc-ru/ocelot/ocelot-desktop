package ocelot.desktop.entity

import ocelot.desktop.entity.traits.OcelotInterface
import totoro.ocelot.brain.Constants
import totoro.ocelot.brain.entity.traits.DeviceInfo.{DeviceAttribute, DeviceClass}
import totoro.ocelot.brain.entity.traits.{DeviceInfo, Tiered}
import totoro.ocelot.brain.network.{Network, Node, Visibility}
import totoro.ocelot.brain.util.Tier
import totoro.ocelot.brain.util.Tier.Tier

class OcelotCard extends OcelotInterface with DeviceInfo with Tiered {
  override val node: Node = Network.newNode(this, Visibility.Neighbors)
    .withComponent("ocelot", Visibility.Neighbors)
    .create()

  override def tier: Tier = Tier.One

  private final lazy val deviceInfo = Map(
    DeviceAttribute.Class -> DeviceClass.Generic,
    DeviceAttribute.Description -> "Yarn ball manipulator",
    DeviceAttribute.Vendor -> Constants.DeviceInfo.DefaultVendor,
    DeviceAttribute.Product -> "Catbus",
  )

  override def getDeviceInfo: Map[String, String] = deviceInfo
}
