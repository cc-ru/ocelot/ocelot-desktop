package ocelot.desktop.entity

import ocelot.desktop.entity.traits.OcelotInterface
import totoro.ocelot.brain.Constants
import totoro.ocelot.brain.entity.traits.DeviceInfo
import totoro.ocelot.brain.entity.traits.DeviceInfo.{DeviceAttribute, DeviceClass}
import totoro.ocelot.brain.network.{Network, Node, Visibility}

class OcelotBlock extends OcelotInterface with DeviceInfo {
  override val node: Node = Network.newNode(this, Visibility.Network)
    .withComponent("ocelot", Visibility.Network)
    .create()

  private final lazy val deviceInfo = Map(
    DeviceAttribute.Class -> DeviceClass.Generic,
    DeviceAttribute.Description -> "Cardboard box simulator",
    DeviceAttribute.Vendor -> Constants.DeviceInfo.DefaultVendor,
    DeviceAttribute.Product -> "Catbus",
  )

  override def getDeviceInfo: Map[String, String] = deviceInfo
}
