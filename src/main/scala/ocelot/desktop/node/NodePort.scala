package ocelot.desktop.node

import ocelot.desktop.ColorScheme
import ocelot.desktop.color.Color
import totoro.ocelot.brain.util.Direction

case class NodePort(direction: Option[Direction.Value] = None) extends Ordered[NodePort] {
  def getColor: Color = direction match {
    case Some(Direction.Down) => ColorScheme("PortDown")
    case Some(Direction.Up) => ColorScheme("PortUp")
    case Some(Direction.North) => ColorScheme("PortNorth")
    case Some(Direction.South) => ColorScheme("PortSouth")
    case Some(Direction.East) => ColorScheme("PortEast")
    case Some(Direction.West) => ColorScheme("PortWest")
    case _ => ColorScheme("PortAny")
  }

  override def compare(that: NodePort): Int = this.direction.compare(that.direction)

  def toByte: Byte = direction match {
    case Some(v) => v.id.toByte
    case None => -1
  }
}

object NodePort {
  def fromByte(byte: Byte): NodePort = {
    if (byte == -1) NodePort(None) else NodePort(Some(Direction(byte)))
  }
}
