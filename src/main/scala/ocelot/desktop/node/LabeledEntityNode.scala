package ocelot.desktop.node

trait LabeledEntityNode extends EntityNode with LabeledNode {
  protected def fallbackLabelAddress: Option[String] = Some(entity.node.address)

  override def label: Option[String] =
    super.label.orElse(if (exposeAddress) fallbackLabelAddress else None)
}
