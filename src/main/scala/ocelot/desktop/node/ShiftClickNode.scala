package ocelot.desktop.node

import ocelot.desktop.ui.event.sources.KeyEvents
import ocelot.desktop.ui.event.{ClickEvent, MouseEvent}

trait ShiftClickNode extends Node {
  protected def onShiftClick(event: ClickEvent): Unit
  protected def hoveredShiftStatusBarText: String

  override def onClick(event: ClickEvent): Unit = {
    event match {
      case ClickEvent(MouseEvent.Button.Left, _) if KeyEvents.isShiftDown =>
        onShiftClick(event)
        return
      case _ =>
    }

    super.onClick(event)
  }

  override def update(): Unit = {
    super.update()

    if (isHovered || isMoving)
      root.get.statusBar.addKeyMouseEntry("icons/LMB", "SHIFT", hoveredShiftStatusBarText)
  }
}
