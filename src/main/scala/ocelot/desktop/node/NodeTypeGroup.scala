package ocelot.desktop.node

import scala.collection.mutable

class NodeTypeGroup(val name: String) {
  val types: mutable.ArrayBuffer[NodeType] = mutable.ArrayBuffer[NodeType]()
}
