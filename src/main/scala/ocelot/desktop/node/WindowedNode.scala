package ocelot.desktop.node

import ocelot.desktop.ui.event.sources.KeyEvents
import ocelot.desktop.ui.event.{ClickEvent, MouseEvent}
import ocelot.desktop.ui.widget.window.{Window, Windowed}
import org.lwjgl.input.Keyboard

trait WindowedNode[T <: Window] extends Node with Windowed[T] {
  override def dispose(): Unit = {
    closeAndDisposeWindow()

    super.dispose()
  }

  override def update(): Unit = {
    if (isHovered || isMoving)
      root.get.statusBar.addMouseEntry("icons/LMB", if (windowCreated && window.isOpen) "Close" else "Open")

    super.update()
  }

  override def onClick(event: ClickEvent): Unit = {
    super.onClick(event)

    event match {
      // FIXME: this. is. ugly. do proper modifier tracking.
      case ClickEvent(MouseEvent.Button.Left, _) if !KeyEvents.isShiftDown =>
        if (windowCreated && window.isOpen)
          window.close()
        else
          window.open()

      case _ =>
    }
  }
}
