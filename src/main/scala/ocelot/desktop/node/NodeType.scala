package ocelot.desktop.node

import ocelot.desktop.graphics.IconSource
import totoro.ocelot.brain.util.Tier.Tier

class NodeType(val name: String, val icon: IconSource, val tier: Option[Tier], factory: => Node)
    extends Ordered[NodeType] {
  def make(): Node = factory

  override def compare(that: NodeType): Int = this.name.compare(that.name)
}

object NodeType {
  def apply(name: String, icon: IconSource, tier: Tier)(factory: => Node): NodeType =
    new NodeType(name, icon, Some(tier), factory)

  def apply(name: String, icon: IconSource, tier: Option[Tier])(factory: => Node): NodeType =
    new NodeType(name, icon, tier, factory)
}
