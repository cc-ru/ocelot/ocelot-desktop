package ocelot.desktop.node.nodes

import ocelot.desktop.color.RGBAColor
import ocelot.desktop.graphics.{Graphics, IconSource}
import ocelot.desktop.node.{EntityNode, LabeledEntityNode}
import ocelot.desktop.ui.event.HoverEvent
import totoro.ocelot.brain.entity.ColorfulLamp

class ColorfulLampNode(val lamp: ColorfulLamp) extends EntityNode(lamp) with LabeledEntityNode {
  private var lastColor: RGBAColor = RGBAColor(0, 0, 0)
  private var mouseHover: Boolean = false

  override def label: Option[String] = super.label.filter(_ => mouseHover)

  override def draw(g: Graphics): Unit = {
    super.draw(g)

    lastColor = RGBAColor(
      (((lamp.color >>> 10) & 0x1f) << 3).toShort,
      (((lamp.color >>> 5) & 0x1f) << 3).toShort,
      (((lamp.color >>> 0) & 0x1f) << 3).toShort,
    )

    g.rect(position.x + 2, position.y + 2, size.width - 4, size.height - 4, lastColor)
    g.sprite(IconSource.Nodes.Lamp.Frame, position.x + 2, position.y + 2, size.width - 4, size.height - 4)
  }

  override def drawLight(g: Graphics): Unit = {
    super.drawLight(g)
    g.sprite(IconSource.Nodes.Lamp.Glow, position - size / 2, size * 2, lastColor)
  }

  eventHandlers += {
    case event: HoverEvent =>
      mouseHover = event.state == HoverEvent.State.Enter
  }
}
