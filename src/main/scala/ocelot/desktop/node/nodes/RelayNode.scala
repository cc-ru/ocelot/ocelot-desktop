package ocelot.desktop.node.nodes

import ocelot.desktop.graphics.IconSource
import ocelot.desktop.inventory.SyncedInventory
import ocelot.desktop.node.{EntityNode, LabeledNode, NodePort, WindowedNode}
import ocelot.desktop.windows.RelayWindow
import totoro.ocelot.brain.entity.Relay
import totoro.ocelot.brain.network
import totoro.ocelot.brain.util.Direction

class RelayNode(val relay: Relay)
    extends EntityNode(relay) with SyncedInventory with LabeledNode with WindowedNode[RelayWindow] {

  override val iconSource: IconSource = IconSource.Nodes.Relay

  override def ports: Array[NodePort] = Array(
    NodePort(Some(Direction.North)),
    NodePort(Some(Direction.South)),
    NodePort(Some(Direction.East)),
    NodePort(Some(Direction.West)),
    NodePort(Some(Direction.Up)),
    NodePort(Some(Direction.Down)),
  )

  override def getNodeByPort(port: NodePort): network.Node = relay.sidedNode(port.direction.get)

  override def exposeAddress = false

  override def shouldReceiveEventsFor(address: String): Boolean = {
    ports.exists(port => getNodeByPort(port).address == address)
  }

  override def brainInventory: Relay = relay

  override protected def createWindow(): RelayWindow = new RelayWindow(this)
}
