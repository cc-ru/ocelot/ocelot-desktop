package ocelot.desktop.node.nodes

import ocelot.desktop.graphics.IconSource
import ocelot.desktop.node.{EntityNode, LabeledEntityNode, WindowedNode}
import ocelot.desktop.windows.HologramProjectorWindow
import totoro.ocelot.brain.entity.HologramProjector

class HologramProjectorNode(val hologramProjector: HologramProjector)
    extends EntityNode(hologramProjector) with LabeledEntityNode with WindowedNode[HologramProjectorWindow] {

  override def iconSource: IconSource = IconSource.Nodes.HologramProjector(hologramProjector.tier)

  override def createWindow(): HologramProjectorWindow = new HologramProjectorWindow(this)
}
