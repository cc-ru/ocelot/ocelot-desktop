package ocelot.desktop.node.nodes

import ocelot.desktop.geometry.Size2D
import ocelot.desktop.graphics.IconSource
import ocelot.desktop.inventory.traits.PersistableItem
import ocelot.desktop.inventory.{Item, PersistedInventory}
import ocelot.desktop.node.{LabeledNode, WindowedNode}
import ocelot.desktop.windows.ChestWindow

class ChestNode extends LabeledNode with WindowedNode[ChestWindow] with PersistedInventory {
  override type I = Item with PersistableItem

  override def iconSource: IconSource = IconSource.Nodes.Chest

  override def minimumSize: Size2D = Size2D(28, 30) * 2 + 4

  override def onItemAdded(slot: Slot): Unit = {}

  override def onItemRemoved(slot: Slot, removedItem: I, replacedBy: Option[I]): Unit = {}

  override def createWindow(): ChestWindow = new ChestWindow(this)
}
