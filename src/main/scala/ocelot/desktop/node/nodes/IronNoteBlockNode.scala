package ocelot.desktop.node.nodes

import ocelot.desktop.graphics.IconSource
import ocelot.desktop.node.nodes.IronNoteBlockNode.{Instruments, NumberOfPitches}
import ocelot.desktop.ui.event.{ClickEvent, MouseEvent}
import totoro.ocelot.brain.entity.IronNoteBlock
import totoro.ocelot.brain.event.{EventBus, NoteBlockTriggerEvent}

import scala.util.Random

class IronNoteBlockNode(val ironNoteBlock: IronNoteBlock) extends NoteBlockNodeBase(ironNoteBlock) {
  override def iconSource: IconSource = IconSource.Nodes.IronNoteBlock

  override def onClick(event: ClickEvent): Unit = {
    event match {
      case ClickEvent(MouseEvent.Button.Left, _) =>
        if (ironNoteBlock != null) {
          EventBus.send(NoteBlockTriggerEvent(
            ironNoteBlock.node.address,
            Instruments(Random.nextInt(Instruments.length))._1,
            Random.nextInt(NumberOfPitches),
          ))
        }
      case _ =>
    }
    super.onClick(event)
  }
}

object IronNoteBlockNode {
  val NumberOfPitches: Int = 24

  val Instruments: List[(String, String)] = List(
    ("harp", "Harp"),
    ("basedrum", "Bass Drum (Stone)"),
    ("snare", "Snare Drum (Sand)"),
    ("hat", "Hi-hat (Glass)"),
    ("bass", "Bass (Wood)"),
    ("flute", "Flute (Clay)"),
    ("bell", "Glockenspiel (Gold)"),
    ("guitar", "Guitar (Wool)"),
    ("chime", "Chimes (Packed Ice)"),
    ("xylophone", "Xylophone (Bone)"),
    ("pling", "Electric Piano (Glowstone)"),
  )
}
