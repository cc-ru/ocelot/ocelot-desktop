package ocelot.desktop.node.nodes

import ocelot.desktop.geometry.Size2D
import ocelot.desktop.graphics.IconSource
import ocelot.desktop.node.EntityNode
import totoro.ocelot.brain.entity.Cable

class CableNode(val cable: Cable) extends EntityNode(cable) {
  override def iconSource: IconSource = IconSource.Nodes.Cable

  override def minimumSize: Size2D = Size2D(36, 36)

  override def exposeAddress: Boolean = false
}
