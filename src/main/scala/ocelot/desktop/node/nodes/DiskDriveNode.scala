package ocelot.desktop.node.nodes

import ocelot.desktop.geometry.Rect2D
import ocelot.desktop.graphics.{Graphics, IconSource}
import ocelot.desktop.inventory.SyncedInventory
import ocelot.desktop.node.Node.{HighlightThickness, NoHighlightSize}
import ocelot.desktop.node.{EntityNode, LabeledEntityNode, ShiftClickNode, WindowedNode}
import ocelot.desktop.ui.event.ClickEvent
import ocelot.desktop.ui.event.handlers.DiskActivityHandler
import ocelot.desktop.ui.widget.contextmenu.ContextMenu
import ocelot.desktop.util.DiskDriveAware
import ocelot.desktop.windows.DiskDriveWindow
import totoro.ocelot.brain.entity.FloppyDiskDrive

class DiskDriveNode(entity: FloppyDiskDrive)
    extends EntityNode(entity)
    with SyncedInventory
    with LabeledEntityNode
    with DiskDriveAware
    with DiskActivityHandler
    with ShiftClickNode
    with WindowedNode[DiskDriveWindow] {

  override def iconSource: IconSource = IconSource.Nodes.DiskDrive.Default

  override def setupContextMenu(menu: ContextMenu, event: ClickEvent): Unit = {
    if (isFloppyItemPresent) {
      addEjectContextMenuEntry(menu)

      menu.addSeparator()
    }

    super.setupContextMenu(menu, event)
  }

  override def draw(g: Graphics): Unit = {
    super.draw(g)

    drawActivityAndFloppy(
      g,
      Rect2D(
        position.x + HighlightThickness,
        position.y + HighlightThickness,
        NoHighlightSize,
        NoHighlightSize,
      ),
      IconSource.Nodes.DiskDrive,
    )
  }

  // -------------------------------- LabeledEntityNode --------------------------------

  override def fallbackLabelAddress: Option[String] = entity.filesystemNode.map(_.address)

  // ---------------------------- DiskDriveAware ----------------------------

  override def floppyDiskDrive: FloppyDiskDrive = entity

  // ---------------------------- ShiftClickNode ----------------------------

  override protected def onShiftClick(event: ClickEvent): Unit = {
    if (isFloppyItemPresent)
      eject()
  }

  override protected def hoveredShiftStatusBarText: String = "Eject floppy"
}
