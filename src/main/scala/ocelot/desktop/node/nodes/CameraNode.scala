package ocelot.desktop.node.nodes

import ocelot.desktop.entity.Camera
import ocelot.desktop.graphics.IconSource
import ocelot.desktop.node.{EntityNode, LabeledEntityNode, WindowedNode}
import ocelot.desktop.windows.CameraWindow

class CameraNode(val camera: Camera) extends EntityNode(camera) with LabeledEntityNode with WindowedNode[CameraWindow] {
  override def iconSource: IconSource = IconSource.Nodes.Camera

  override def createWindow(): CameraWindow = new CameraWindow(this)
}
