package ocelot.desktop.util

import org.apache.logging.log4j.{Logger, LogManager}

trait Logging {
  protected val logger: Logger = LogManager.getLogger(this.getClass)
}
