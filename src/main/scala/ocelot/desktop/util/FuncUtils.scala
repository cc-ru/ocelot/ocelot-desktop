package ocelot.desktop.util

object FuncUtils {
  def composeN[A](n: Int)(f: A => A): A => A = Function.chain(List.fill(n)(f))
}
