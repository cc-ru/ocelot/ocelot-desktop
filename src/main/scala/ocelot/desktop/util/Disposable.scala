package ocelot.desktop.util

trait Disposable {
  def dispose(): Unit = {}
}
