package ocelot.desktop.util

import org.apache.commons.lang3.SystemUtils

import java.nio.file.{Path, Paths}
import java.util.Objects

object OcelotPaths {
  def windowsAppDataDirectoryName: String = Objects.requireNonNull(System.getenv("APPDATA"), "%APPDATA% is null")
  def linuxHomeDirectoryName: String = Objects.requireNonNull(SystemUtils.USER_HOME, "USER_HOME is null")

  def openComputersConfigName: String = {
    if (SystemUtils.IS_OS_WINDOWS)
      "OpenComputers.conf"
    else
      "opencomputers.conf"
  }

  def libraries: Path = {
    if (SystemUtils.IS_OS_WINDOWS)
      Paths.get(windowsAppDataDirectoryName, "Ocelot", "Libraries")
    else
      Paths.get(linuxHomeDirectoryName, ".local", "lib", "ocelot")
  }

  def openComputersConfig: Path = {
    if (SystemUtils.IS_OS_WINDOWS)
      Paths.get(windowsAppDataDirectoryName, "Ocelot", "Config", openComputersConfigName)
    else
      Paths.get(linuxHomeDirectoryName, ".config", "ocelot", openComputersConfigName)
  }

  def desktopConfig: Path = {
    if (SystemUtils.IS_OS_WINDOWS)
      Paths.get(windowsAppDataDirectoryName, "Ocelot", "Config", "Desktop.conf")
    else
      Paths.get(linuxHomeDirectoryName, ".config", "ocelot", "desktop.conf")
  }

  def desktopLog: Path = {
    if (SystemUtils.IS_OS_WINDOWS)
      Paths.get(windowsAppDataDirectoryName, "Ocelot", "Logs", "Desktop.log")
    else
      Paths.get(linuxHomeDirectoryName, ".local", "share", "ocelot", "desktop.log")
  }
}
