package ocelot.desktop.util

import java.util.concurrent.LinkedBlockingQueue

/** A thread-safe queue of tasks that should be run in owner thread.
  * Works the same way as Platform.runLater() from JavaFX.
  */
class TaskQueue {
  private val queue = new LinkedBlockingQueue[() => Unit]()

  def add(task: () => Unit): Unit = queue.add(task)

  def run(): Unit = {
    while (!queue.isEmpty)
      queue.take()()
  }
}
