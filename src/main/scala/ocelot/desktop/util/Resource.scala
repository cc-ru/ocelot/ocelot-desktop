package ocelot.desktop.util

trait Resource {
  ResourceManager.registerResource(this)

  def freeResource(): Unit = {
    ResourceManager.unregisterResource(this)
  }
}
