package ocelot.desktop.util

import ocelot.desktop.inventory.Inventory

trait DefaultSlotItemsFillable extends Inventory {
  def fillSlotsWithDefaultItems(): Unit
}
