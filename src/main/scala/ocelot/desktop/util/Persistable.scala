package ocelot.desktop.util

import totoro.ocelot.brain.nbt.NBTTagCompound
import totoro.ocelot.brain.util.{Persistable => BrainPersistable}
import totoro.ocelot.brain.workspace.Workspace

/** Akin to [[BrainPersistable the brain's Persistable]] except it doesn't care about workspaces. */
trait Persistable extends BrainPersistable {
  def load(nbt: NBTTagCompound): Unit = {}

  override final def load(nbt: NBTTagCompound, workspace: Workspace): Unit = load(nbt)

  override def save(nbt: NBTTagCompound): Unit = {}
}
