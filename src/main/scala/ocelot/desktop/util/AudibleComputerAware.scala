package ocelot.desktop.util

import ocelot.desktop.audio._

trait AudibleComputerAware extends ComputerAware {
  lazy val soundComputerRunning: SoundSource = SoundSource.fromBuffer(
    SoundBuffers.MachineComputerRunning,
    SoundCategory.Environment,
    looping = true,
  )

  def updateRunningSound(): Unit = {
    if (!computer.machine.isRunning && soundComputerRunning.isPlaying) {
      soundComputerRunning.stop()
    } else if (computer.machine.isRunning && !soundComputerRunning.isPlaying && !Audio.isDisabled) {
      soundComputerRunning.play()
    }
  }

  override def turnOn(): Unit = {
    super.turnOn()

    soundComputerRunning.play()
  }

  override def turnOff(): Unit = {
    super.turnOff()

    soundComputerRunning.stop()
  }
}
