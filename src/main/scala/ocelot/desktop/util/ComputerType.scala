package ocelot.desktop.util

object ComputerType extends Enumeration {
  type ComputerType = Value

  val Computer, Server, Microcontroller = Value
}
