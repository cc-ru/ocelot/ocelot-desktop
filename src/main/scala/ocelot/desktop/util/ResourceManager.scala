package ocelot.desktop.util

import scala.collection.mutable.ArrayBuffer

object ResourceManager extends Logging {
  private val resources = new ArrayBuffer[Resource]

  def registerResource(resource: Resource): Unit = {
    resources += resource
  }

  def unregisterResource(resource: Resource): Unit = {
    resources -= resource
  }

  def checkEmpty(): Unit = {
    if (resources.isEmpty)
      return

    logger.error(s"${resources.length} resources have not been freed properly")

    for ((groupClass, groupResources) <- resources.groupBy(_.getClass)) {
      logger.error(s" - ${groupResources.length} resources of type ${groupClass.getName}")
    }
  }
}
