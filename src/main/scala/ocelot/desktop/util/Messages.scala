package ocelot.desktop.util

import scala.collection.mutable
import scala.io.Source

object Messages extends Logging with PartialFunction[String, String] {
  private val entries = new mutable.HashMap[String, String]()

  override def apply(key: String): String = entries(key)

  override def isDefinedAt(key: String): Boolean = entries.isDefinedAt(key)

  def load(source: Source): Unit = {
    logger.info(s"Loading messages")

    val oldSize = entries.size

    for (line_ <- source.getLines) {
      val line = line_.trim
      if (line.nonEmpty) {
        val split = line.split("\\s*=\\s*")
        val key = split(0)
        val value = split(1)
        entries(key) = value
      }
    }

    logger.info(s"Loaded ${entries.size - oldSize} messages")
  }
}
