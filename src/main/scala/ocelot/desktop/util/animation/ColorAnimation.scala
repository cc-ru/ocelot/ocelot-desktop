package ocelot.desktop.util.animation

import ocelot.desktop.color.{Color, RGBAColorNorm}
import ocelot.desktop.util.animation.easing.EasingFunction

class ColorAnimation(init: Color, _speed: Float = 6f) {
  private val initRGBA = init.toRGBANorm
  private val r = new ValueAnimation(initRGBA.r, _speed)
  private val g = new ValueAnimation(initRGBA.g, _speed)
  private val b = new ValueAnimation(initRGBA.b, _speed)
  private val a = new ValueAnimation(initRGBA.a, _speed)

  def color: Color = RGBAColorNorm(r.value, g.value, b.value, a.value)

  def easing: EasingFunction = a.easing

  def easing_=(value: EasingFunction): Unit = {
    r.easing = value
    g.easing = value
    b.easing = value
    a.easing = value
  }

  def speed: Float = a.speed

  def speed_=(value: Float): Unit = {
    r.speed = value
    g.speed = value
    b.speed = value
    a.speed = value
  }

  def jump(to: Color): Unit = {
    val col = to.toRGBANorm
    r.jump(col.r)
    g.jump(col.g)
    b.jump(col.b)
    a.jump(col.a)
  }

  def goto(to: Color): Unit = {
    val col = to.toRGBANorm
    r.goto(col.r)
    g.goto(col.g)
    b.goto(col.b)
    a.goto(col.a)
  }

  def update(): Unit = {
    r.update()
    g.update()
    b.update()
    a.update()
  }
}
