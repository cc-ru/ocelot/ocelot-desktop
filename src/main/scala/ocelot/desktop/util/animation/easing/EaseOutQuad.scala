package ocelot.desktop.util.animation.easing

object EaseOutQuad extends EasingFunction {
  override def derivative(t: Float): Float = 2 - 2 * t

  override def apply(t: Float): Float = t * (2 - t)
}
