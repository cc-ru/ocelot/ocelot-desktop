package ocelot.desktop.util.animation.easing

object EaseOutCircular extends EasingFunction {
  override def derivative(t: Float): Float = ((1 - t) / math.sqrt(1 - math.pow(t - 1, 2))).toFloat

  override def apply(t: Float): Float = math.sqrt(1 - math.pow(t - 1, 2)).toFloat
}
