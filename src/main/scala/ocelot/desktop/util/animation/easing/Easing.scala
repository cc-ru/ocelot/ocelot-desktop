package ocelot.desktop.util.animation.easing

object Easing {
  def linear: EasingFunction = Linear

  def easeInQuad: EasingFunction = EaseInQuad

  def easeOutQuad: EasingFunction = EaseOutQuad

  def easeInOutQuad: EasingFunction = EaseInOutQuad

  def easeOutCircular: EasingFunction = EaseOutCircular
}
