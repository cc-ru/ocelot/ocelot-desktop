package ocelot.desktop.color

case class RGBAColor(r: Short, g: Short, b: Short, a: Short = 255) extends Color {
  assert(0 <= r && r <= 255, "Invalid RED channel")
  assert(0 <= g && g <= 255, "Invalid GREEN channel")
  assert(0 <= b && b <= 255, "Invalid BLUE channel")
  assert(0 <= a && a <= 255, "Invalid ALPHA channel")

  override def toInt: IntColor = IntColor((r << 24) + (g << 16) + b)

  override def toRGBA: RGBAColor = this

  override def toRGBANorm: RGBAColorNorm = RGBAColorNorm(
    r.toFloat / 255f,
    g.toFloat / 255f,
    b.toFloat / 255f,
    a.toFloat / 255f,
  )

  override def toHSVA: HSVAColor = toRGBANorm.toHSVA
}
