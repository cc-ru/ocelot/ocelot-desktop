package ocelot.desktop.color

case class IntColor(color: Int) extends Color {
  override def toInt: IntColor = this

  override def toRGBA: RGBAColor = {
    RGBAColor(
      (color >> 16).toShort,
      ((color >> 8) & 0xff).toShort,
      (color & 0xff).toShort,
    )
  }

  override def toRGBANorm: RGBAColorNorm = toRGBA.toRGBANorm

  override def toHSVA: HSVAColor = toRGBANorm.toHSVA

  def withAlpha(a: Float): RGBAColorNorm = toRGBANorm.withAlpha(a)
}
