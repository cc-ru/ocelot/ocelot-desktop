package ocelot.desktop.ui

object Const {
  val AnimationSpeedHoverEnter = 6.0f
  val AnimationSpeedHoverLeave = 0.6f
  val SlotHighlightAlpha = 0.3f
  val SlotHighlightAnimationSpeedEnter = 10f
  val SlotHighlightAnimationSpeedLeave = 2f
}
