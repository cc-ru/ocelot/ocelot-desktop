package ocelot.desktop.ui.swing

import java.awt.Point
import java.awt.event.{MouseAdapter, MouseEvent}
import javax.swing.JDialog

class DialogDragListener(val dialog: JDialog) extends MouseAdapter {
  private var mouseDownCompCoords: Option[Point] = None

  override def mouseReleased(event: MouseEvent): Unit = {
    mouseDownCompCoords = None
  }

  override def mousePressed(event: MouseEvent): Unit = {
    mouseDownCompCoords = Option(event.getPoint)
  }

  override def mouseDragged(event: MouseEvent): Unit = {
    val currentCoords = event.getLocationOnScreen
    dialog.setLocation(currentCoords.x - mouseDownCompCoords.get.x, currentCoords.y - mouseDownCompCoords.get.y)
  }
}
