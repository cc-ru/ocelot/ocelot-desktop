package ocelot.desktop.ui.swing

import java.awt.{Color, Graphics}
import javax.swing.JPanel

class ColoredPanel(color: Color) extends JPanel {
  override protected def paintComponent(g: Graphics): Unit = {
    super.paintComponent(g)
    g.setColor(color)
    g.drawRect(0, 0, getWidth, getHeight)
  }
}
