package ocelot.desktop.ui.swing

import java.awt.Graphics
import java.awt.image.BufferedImage
import javax.imageio.ImageIO
import javax.swing.JPanel

class ImagePanel(imagePath: String) extends JPanel {
  val image: BufferedImage = ImageIO.read(getClass.getResource(imagePath))

  override protected def paintComponent(g: Graphics): Unit = {
    super.paintComponent(g)
    g.drawImage(image, 0, 0, this)
  }
}
