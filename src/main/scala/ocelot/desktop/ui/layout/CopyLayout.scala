package ocelot.desktop.ui.layout

import ocelot.desktop.geometry.Size2D
import ocelot.desktop.ui.widget.Widget

class CopyLayout(widget: Widget) extends Layout(widget) {
  override def recalculateBounds(): Unit = {
    minimumSize = widget.children.map(_.minimumSize).fold(Size2D.Zero)(_.max(_))
    maximumSize = widget.children.map(_.maximumSize).fold(Size2D.Inf)(_.min(_))
  }

  override def relayout(): Unit = {
    for (child <- widget.children) {
      child.rawSetPosition(widget.position)
      child.rawSetSize(widget.size)
      child.relayout()
    }
  }
}
