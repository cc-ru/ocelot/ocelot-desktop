package ocelot.desktop.ui.event

object HoverEvent {
  object State extends Enumeration {
    val Enter, Leave = Value
  }
}

case class HoverEvent(state: HoverEvent.State.Value) extends Event
