package ocelot.desktop.ui.event.handlers

import ocelot.desktop.audio.{Audio, SoundBuffers, SoundCategory, SoundSource}
import ocelot.desktop.ui.event.{BrainEvent, EventAware}
import totoro.ocelot.brain.event.FileSystemActivityEvent
import totoro.ocelot.brain.event.FileSystemActivityType.Floppy

import scala.util.Random

trait DiskActivityHandler extends EventAware {
  eventHandlers += {
    case BrainEvent(event: FileSystemActivityEvent) if !Audio.isDisabled =>
      val sound = {
        if (event.activityType == Floppy)
          SoundBuffers.MachineFloppyAccess
            .map(buffer => SoundSource.fromBuffer(buffer, SoundCategory.Environment))
        else
          SoundBuffers.MachineHDDAccess
            .map(buffer => SoundSource.fromBuffer(buffer, SoundCategory.Environment))
      }

      sound(Random.between(0, sound.length)).play()
  }
}
