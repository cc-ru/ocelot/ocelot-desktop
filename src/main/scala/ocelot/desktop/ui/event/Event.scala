package ocelot.desktop.ui.event

trait Event extends AnyRef {
  private var _consumed: Boolean = false

  def consume(): Unit = {
    _consumed = true
  }

  def consumed: Boolean = _consumed
}
