package ocelot.desktop.ui.event

import ocelot.desktop.ui.widget.EventHandlers

trait EventAware {
  protected val eventHandlers = new EventHandlers

  def shouldReceiveEventsFor(address: String): Boolean = false

  def handleEvent(event: Event): Unit = {
    eventHandlers(event)
  }
}
