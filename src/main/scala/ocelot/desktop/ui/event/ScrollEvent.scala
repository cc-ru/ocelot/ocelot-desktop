package ocelot.desktop.ui.event

case class ScrollEvent(offset: Int) extends Event
