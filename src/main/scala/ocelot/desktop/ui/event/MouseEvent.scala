package ocelot.desktop.ui.event

object MouseEvent {
  object State extends Enumeration {
    val Press, Release = Value
  }

  object Button extends Enumeration {
    val Left: Button.Value = Value(0)
    val Right: Button.Value = Value(1)
    val Middle: Button.Value = Value(2)
  }
}

case class MouseEvent(state: MouseEvent.State.Value, button: MouseEvent.Button.Value) extends Event
