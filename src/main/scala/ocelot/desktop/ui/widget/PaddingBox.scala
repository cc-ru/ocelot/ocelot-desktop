package ocelot.desktop.ui.widget

import ocelot.desktop.geometry.Padding2D
import ocelot.desktop.ui.layout.PaddingLayout

import scala.collection.immutable.ArraySeq

class PaddingBox(inner: Widget, padding: Padding2D) extends Widget {
  override protected val layout = new PaddingLayout(this, padding)

  children = ArraySeq(inner)
}
