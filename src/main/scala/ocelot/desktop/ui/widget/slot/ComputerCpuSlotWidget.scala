package ocelot.desktop.ui.widget.slot

import ocelot.desktop.inventory.traits.CpuLikeItem
import ocelot.desktop.inventory.{Inventory, Item}
import ocelot.desktop.util.ComputerAware
import totoro.ocelot.brain.util.Tier.Tier

class ComputerCpuSlotWidget(slot: Inventory#Slot, computer: ComputerAware, _tier: Tier)
    extends CpuSlotWidget(slot, _tier) {
  override protected def onItemNotification(notification: Item.Notification): Unit = {
    super.onItemNotification(notification)

    notification match {
      case CpuLikeItem.CpuArchitectureChangedNotification =>
        computer.turnOn()
        computer.turnOff()

      case _ =>
    }
  }
}
