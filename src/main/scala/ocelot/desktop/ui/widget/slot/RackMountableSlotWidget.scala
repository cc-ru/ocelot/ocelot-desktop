package ocelot.desktop.ui.widget.slot

import ocelot.desktop.graphics.IconSource
import ocelot.desktop.inventory.Inventory
import ocelot.desktop.inventory.item.ServerItem
import ocelot.desktop.inventory.traits.RackMountableItem

class RackMountableSlotWidget(slot: Inventory#Slot) extends SlotWidget[RackMountableItem](slot) {
  override def ghostIcon: Option[IconSource] = Some(IconSource.ServerIcon)

  override def onItemRemoved(removedItem: RackMountableItem, replacedBy: Option[RackMountableItem]): Unit = {
    super.onItemRemoved(removedItem, replacedBy)

    removedItem match {
      case serverItem: ServerItem => serverItem.onRemoved()
      case _ =>
    }
  }
}
