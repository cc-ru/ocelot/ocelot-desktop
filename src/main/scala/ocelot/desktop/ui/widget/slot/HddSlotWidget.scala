package ocelot.desktop.ui.widget.slot

import ocelot.desktop.graphics.IconSource
import ocelot.desktop.inventory.Inventory
import ocelot.desktop.inventory.item.HddItem
import totoro.ocelot.brain.util.Tier.Tier

class HddSlotWidget(slot: Inventory#Slot, _tier: Tier) extends SlotWidget[HddItem](slot) {
  override def ghostIcon: Option[IconSource] = Some(IconSource.HddIcon)
  override def slotTier: Option[Tier] = Some(_tier)
}
