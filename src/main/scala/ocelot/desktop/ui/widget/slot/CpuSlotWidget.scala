package ocelot.desktop.ui.widget.slot

import ocelot.desktop.graphics.IconSource
import ocelot.desktop.inventory.Inventory
import ocelot.desktop.inventory.traits.CpuLikeItem
import totoro.ocelot.brain.util.Tier.Tier

class CpuSlotWidget(slot: Inventory#Slot, _tier: Tier) extends SlotWidget[CpuLikeItem](slot) {
  override def ghostIcon: Option[IconSource] = Some(IconSource.CpuIcon)
  override def slotTier: Option[Tier] = Some(_tier)
}
