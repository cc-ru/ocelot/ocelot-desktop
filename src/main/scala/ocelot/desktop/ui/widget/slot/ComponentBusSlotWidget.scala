package ocelot.desktop.ui.widget.slot

import ocelot.desktop.graphics.IconSource
import ocelot.desktop.inventory.Inventory
import ocelot.desktop.inventory.item.ComponentBusItem
import totoro.ocelot.brain.util.Tier.Tier

class ComponentBusSlotWidget(slot: Inventory#Slot, _tier: Tier) extends SlotWidget[ComponentBusItem](slot) {
  override def ghostIcon: Option[IconSource] = Some(IconSource.ComponentBusIcon)
  override def slotTier: Option[Tier] = Some(_tier)
}
