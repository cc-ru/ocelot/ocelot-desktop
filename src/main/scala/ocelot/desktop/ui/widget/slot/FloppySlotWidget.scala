package ocelot.desktop.ui.widget.slot

import ocelot.desktop.audio.{Audio, SoundSource}
import ocelot.desktop.graphics.IconSource
import ocelot.desktop.inventory.Inventory
import ocelot.desktop.inventory.item.FloppyItem

class FloppySlotWidget(slot: Inventory#Slot) extends SlotWidget[FloppyItem](slot) {
  override def ghostIcon: Option[IconSource] = Some(IconSource.FloppyIcon)

  override def onItemAdded(): Unit = {
    super.onItemAdded()

    // TODO: don't play the sound when loading a workspace
    if (!Audio.isDisabled)
      SoundSource.MachineFloppyInsert.play()
  }

  override def onItemRemoved(
    removedItem: FloppyItem,
    replacedBy: Option[FloppyItem],
  ): Unit = {
    super.onItemRemoved(removedItem, replacedBy)

    // TODO: don't play the sound when loading a workspace
    if (!Audio.isDisabled)
      SoundSource.MachineFloppyEject.play()
  }
}
