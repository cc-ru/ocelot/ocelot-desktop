package ocelot.desktop.ui.widget.slot

import ocelot.desktop.graphics.IconSource
import ocelot.desktop.inventory.Inventory
import ocelot.desktop.inventory.item.EepromItem

class EepromSlotWidget(slot: Inventory#Slot) extends SlotWidget[EepromItem](slot) {
  override def ghostIcon: Option[IconSource] = Some(IconSource.EepromIcon)
}
