package ocelot.desktop.ui.widget.slot

import ocelot.desktop.graphics.IconSource
import ocelot.desktop.inventory.Inventory
import ocelot.desktop.inventory.traits.CardItem
import totoro.ocelot.brain.util.Tier.Tier

class CardSlotWidget(slot: Inventory#Slot, _tier: Tier) extends SlotWidget[CardItem](slot) {
  override def ghostIcon: Option[IconSource] = Some(IconSource.CardIcon)
  override def slotTier: Option[Tier] = Some(_tier)
}
