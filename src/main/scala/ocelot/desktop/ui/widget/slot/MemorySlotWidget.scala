package ocelot.desktop.ui.widget.slot

import ocelot.desktop.graphics.IconSource
import ocelot.desktop.inventory.Inventory
import ocelot.desktop.inventory.item.MemoryItem
import totoro.ocelot.brain.util.Tier.Tier

class MemorySlotWidget(slot: Inventory#Slot, _tier: Tier) extends SlotWidget[MemoryItem](slot) {
  override def ghostIcon: Option[IconSource] = Some(IconSource.MemoryIcon)
  override def slotTier: Option[Tier] = Some(_tier)
}
