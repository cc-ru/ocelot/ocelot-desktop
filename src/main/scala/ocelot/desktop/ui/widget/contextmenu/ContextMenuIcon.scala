package ocelot.desktop.ui.widget.contextmenu

import ocelot.desktop.color.Color
import ocelot.desktop.graphics.IconSource

case class ContextMenuIcon(
  source: IconSource,
  size: ContextMenuIconSize.Value = ContextMenuIconSize.Normal,
  color: Option[Color] = None,
)
