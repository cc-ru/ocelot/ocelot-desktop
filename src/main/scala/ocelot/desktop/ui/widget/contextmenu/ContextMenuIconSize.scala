package ocelot.desktop.ui.widget.contextmenu

object ContextMenuIconSize extends Enumeration {
  val Normal, Big = Value
}
