package ocelot.desktop.ui.widget

import ocelot.desktop.ColorScheme
import ocelot.desktop.color.Color
import ocelot.desktop.geometry.Vector2D
import ocelot.desktop.node.Node

class ComputerErrorMessageLabel(node: Node, override val text: String) extends Label {
  override def isSmall: Boolean = true

  var alpha: Float = 1f

  override def color: Color = ColorScheme("ErrorMessage").toRGBANorm.mapA(_ * alpha)

  val initialPosition: Vector2D = {
    val position = node.position
    val size = node.size

    position + Vector2D(size.width / 2 - minimumSize.width / 2, -minimumSize.height)
  }

  position = initialPosition
}
