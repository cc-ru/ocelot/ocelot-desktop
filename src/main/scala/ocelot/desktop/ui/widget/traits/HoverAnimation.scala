package ocelot.desktop.ui.widget.traits

import ocelot.desktop.ColorScheme
import ocelot.desktop.color.Color
import ocelot.desktop.ui.Const._
import ocelot.desktop.ui.event.{EventAware, HoverEvent}
import ocelot.desktop.ui.event.handlers.HoverHandler
import ocelot.desktop.ui.widget.{Updatable, Widget}
import ocelot.desktop.util.animation.ColorAnimation

/** Helper trait that manages color animations for UI widgets that should be highlighted on mouse hover. */

//noinspection ScalaWeakerAccess

trait HoverAnimation extends Widget with EventAware with HoverHandler with Updatable {
  protected val hoverAnimationSpeedEnter: Float = AnimationSpeedHoverEnter
  protected val hoverAnimationSpeedLeave: Float = AnimationSpeedHoverLeave
  protected val hoverAnimationColorDefault: Color = ColorScheme("ButtonBackground")
  protected val hoverAnimationColorActive: Color = ColorScheme("ButtonBackgroundActive")

  protected lazy val hoverAnimation: ColorAnimation =
    new ColorAnimation(hoverAnimationColorDefault, hoverAnimationSpeedEnter)

  eventHandlers += {
    case HoverEvent(HoverEvent.State.Enter) =>
      startHoverEnterAnimation()

    case HoverEvent(HoverEvent.State.Leave) =>
      startHoverLeaveAnimation()
  }

  protected def startHoverEnterAnimation(): Unit = {
    hoverAnimation.speed = hoverAnimationSpeedEnter
    hoverAnimation.goto(hoverAnimationColorActive)
  }

  protected def startHoverLeaveAnimation(): Unit = {
    hoverAnimation.speed = hoverAnimationSpeedLeave
    hoverAnimation.goto(hoverAnimationColorDefault)
  }

  override def update(): Unit = {
    super.update()
    hoverAnimation.update()
  }
}
