package ocelot.desktop.ui.widget.tooltip

import ocelot.desktop.Settings
import ocelot.desktop.color.Color
import ocelot.desktop.geometry.Padding2D
import ocelot.desktop.ui.widget.Label

class LabelTooltip(text: String) extends Tooltip {
  override val DelayTime: Float = Settings.get.tooltipDelayUI

  for (line <- text.split("\n")) {
    body.children :+= new Label {
      override def text: String = line
      override def color: Color = Color.White
    }
  }

  override def bodyPadding: Padding2D = Padding2D.equal(4)

  def onSaveSelected(): Unit = {}

  def onExitSelected(): Unit = {}
}
