package ocelot.desktop.ui.widget.tooltip

import ocelot.desktop.Settings
import ocelot.desktop.color.Color
import ocelot.desktop.geometry.Padding2D
import ocelot.desktop.ui.widget.Label

class ItemTooltip extends Tooltip {
  override val DelayTime: Float = Settings.get.tooltipDelayItem

  override def bodyPadding: Padding2D = Padding2D.equal(5)

  def addLine(_text: String, _color: Color = Color.Grey): Unit = {
    body.children :+= new Label {
      override def text: String = _text
      override def color: Color = _color
    }
  }
}
