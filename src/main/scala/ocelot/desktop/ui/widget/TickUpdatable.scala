package ocelot.desktop.ui.widget

import ocelot.desktop.OcelotDesktop

trait TickUpdatable extends Updatable {
  private var lastTick = OcelotDesktop.ticker.tick

  def tickUpdate(): Unit = {}

  override def update(): Unit = {
    super.update()

    val currentTick = OcelotDesktop.ticker.tick

    if (currentTick > lastTick) {
      lastTick = currentTick
      tickUpdate()
    }
  }
}
