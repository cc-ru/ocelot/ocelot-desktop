package ocelot.desktop.ui.widget

import ocelot.desktop.audio.SoundSource
import ocelot.desktop.geometry.{Padding2D, Size2D}
import ocelot.desktop.graphics.{Graphics, IconSource}
import ocelot.desktop.ui.event.KeyEvent
import ocelot.desktop.ui.widget.contextmenu.{ContextMenuEntry, ContextMenuSubmenu}
import ocelot.desktop.ui.widget.help.{AboutDialog, UpdateCheckerDialog}
import ocelot.desktop.ui.widget.settings.SettingsDialog
import ocelot.desktop.{ColorScheme, OcelotDesktop}
import org.lwjgl.input.Keyboard

class MenuBar extends Widget {
  override def receiveMouseEvents: Boolean = true

  private val entries = new Widget {}

  children :+= new PaddingBox(entries, Padding2D(left = 1, right = 1, bottom = 1))

  private def addEntry(w: Widget): Unit = entries.children :+= w

  addEntry(new MenuBarSubmenu(
    "File",
    menu => {
      menu.addEntry(ContextMenuEntry("New", IconSource.Plus) { OcelotDesktop.newWorkspace() })
      menu.addEntry(ContextMenuEntry("Open", IconSource.Folder) { OcelotDesktop.showOpenDialog() })
      menu.addEntry(ContextMenuEntry("Save", IconSource.Save) { OcelotDesktop.save() })
      menu.addEntry(ContextMenuEntry("Save as", IconSource.SaveAs) { OcelotDesktop.saveAs() })

      menu.addSeparator()

      menu.addEntry(ContextMenuEntry("Exit", IconSource.Cross, SoundSource.InterfaceClickLow) { OcelotDesktop.exit() })
    },
  ))

  addEntry(new MenuBarSubmenu(
    "Player",
    menu => {
      menu.addEntry(ContextMenuEntry("Create new", IconSource.Plus) { OcelotDesktop.showAddPlayerDialog() })
      menu.addSeparator()
      OcelotDesktop.players.foreach(player => {
        menu.addEntry(new ContextMenuSubmenu(
          s"${if (player == OcelotDesktop.players.head) "● " else "  "}${player.nickname}",
          onClick = () => OcelotDesktop.selectPlayer(player.nickname),
        ) {
          addEntry(ContextMenuEntry("Remove", IconSource.Delete, SoundSource.InterfaceClickLow) {
            OcelotDesktop.removePlayer(player.nickname)
          })
        })
      })
    },
  ))

  addEntry(new MenuBarButton("Settings", () => new SettingsDialog().show()))

  addEntry(new MenuBarSubmenu(
    "Help",
    menu => {
      menu.addEntry(ContextMenuEntry("Check for Updates", IconSource.Help) {
        new UpdateCheckerDialog().show()
      })
      menu.addSeparator()
      menu.addEntry(ContextMenuEntry("Manual", IconSource.Book) {}.setEnabled(false))
      menu.addEntry(ContextMenuEntry("About", IconSource.Ocelot) { new AboutDialog().show() })
    },
  ))

  addEntry(new Widget {
    override def maximumSize: Size2D = Size2D(Float.PositiveInfinity, 1)
  }) // fill remaining space

  eventHandlers += {
    case KeyEvent(KeyEvent.State.Press, Keyboard.KEY_F5, _) => OcelotDesktop.save()
    case KeyEvent(KeyEvent.State.Press, Keyboard.KEY_F9, _) => OcelotDesktop.showOpenDialog()
  }

  override def draw(g: Graphics): Unit = {
    g.rect(bounds, ColorScheme("TitleBarBackground"))
    drawChildren(g)
    g.rect(bounds.x, bounds.y + height - 1, width, height, ColorScheme("TitleBarBorder"))
  }
}
