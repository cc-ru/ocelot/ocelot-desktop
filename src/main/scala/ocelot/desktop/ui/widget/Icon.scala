package ocelot.desktop.ui.widget

import ocelot.desktop.color.Color
import ocelot.desktop.geometry.Size2D
import ocelot.desktop.graphics.{Graphics, IconSource}
import ocelot.desktop.util.Spritesheet

class Icon(icon: IconSource, size: Size2D = null, private val color: Color = Color.White) extends Widget {
  def this(name: String) {
    this(IconSource(name))
  }

  def this(name: String, animation: IconSource.Animation) {
    this(IconSource(name, animation = Some(animation)))
  }

  def iconColor: Color = color

  def iconSize: Size2D = {
    if (size != null) size
    else if (icon.animation.isDefined && icon.animation.get.frameSize.isDefined)
      icon.animation.get.frameSize.get
    else Spritesheet.spriteSize(icon.path)
  }

  override def minimumSize: Size2D = iconSize
  override def maximumSize: Size2D = minimumSize

  override def draw(g: Graphics): Unit = {
    g.sprite(icon.path, bounds.x, bounds.y, bounds.w, bounds.h, iconColor, icon.animation)
  }
}
