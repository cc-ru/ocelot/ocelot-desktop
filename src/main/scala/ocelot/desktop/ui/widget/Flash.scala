package ocelot.desktop.ui.widget

import ocelot.desktop.ColorScheme
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.util.animation.ValueAnimation
import ocelot.desktop.util.animation.easing.Easing

class Flash(val parent: Widget) {
  private val alpha = new ValueAnimation(1f, speed = 2.5f)
  alpha.easing = Easing.easeOutCircular

  def bang(): Unit = {
    alpha.jump(0f)
    alpha.goto(1f)
  }

  def draw(g: Graphics): Unit = {
    if (alpha.value < 1.0f) {
      val position = parent.position
      val size = parent.size
      val color = ColorScheme("Flash").withAlpha(math.sin(alpha.value * math.Pi).toFloat)
      g.rect(position.x, position.y, size.width, size.height, color)
      alpha.update()
    }
  }
}
