package ocelot.desktop.ui.widget

import ocelot.desktop.geometry.Size2D

class Filler extends Widget {
  override def maximumSize: Size2D = Size2D.Inf
}
