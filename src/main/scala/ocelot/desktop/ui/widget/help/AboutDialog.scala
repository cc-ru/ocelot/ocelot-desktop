package ocelot.desktop.ui.widget.help

import buildinfo.BuildInfo
import ocelot.desktop.ColorScheme
import ocelot.desktop.color.Color
import ocelot.desktop.geometry.{Padding2D, Size2D}
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.ui.layout.LinearLayout
import ocelot.desktop.ui.widget.{Button, Filler, Label, PaddingBox, Widget}
import ocelot.desktop.ui.widget.modal.ModalDialog
import ocelot.desktop.ui.widget.tooltip.LabelTooltip
import ocelot.desktop.util.{DrawUtils, Orientation, Spritesheet}

import java.awt.Desktop
import java.net.URI
import java.time.Year

class AboutDialog extends ModalDialog {
  val WebsiteURL: String = "https://ocelot.fomalhaut.me/desktop"
  val IrcURL: String = "https://webchat.esper.net/?join=cc.ru"
  val DiscordURL: String = "https://discord.gg/ygxUkxg9pt"

  children :+= new Widget {
    private val logo: Widget = new Widget {
      override def minimumSize: Size2D = Spritesheet.spriteSize("Logo") + 40

      override def draw(g: Graphics): Unit = {
        g.sprite("Logo", bounds.x + 20, bounds.y + 80, ColorScheme("AboutLogo"))
        drawChildren(g)
      }
    }
    children :+= logo

    children :+= new PaddingBox(
      new Widget {
        override val layout = new LinearLayout(this, orientation = Orientation.Vertical, gap = 8)

        children :++= Seq(
          new PaddingBox(AboutLabel("OCELOT DESKTOP"), Padding2D(top = 32)),
          new PaddingBox(AboutLabel("  OpenComputers Emulator", isSmall = true), Padding2D(bottom = 16)),
          AboutLabel(s"Version: ${BuildInfo.version}"),
          new PaddingBox(AboutLabel(s"  (${BuildInfo.commit.take(7)})", isSmall = true), Padding2D(bottom = 16)),
          AboutLabel(s"MIT License (c) ${Year.now.getValue} Ocelot Dev Team"),
          AboutLabel("  OpenComputers (C) 2013-2015, MIT, Florian \"Sangar\" Nücke", isSmall = true),
          AboutLabel("  MineOS (C) 2022, Dungeon MIT License, ECS", isSmall = true),
          AboutLabel("  Advanced Loader (C) 2018, MIT, Luca_S", isSmall = true),
          AboutLabel("  Unifont (C) 2007, SIL OFL 1.1, Roman Czyborra", isSmall = true),
          AboutLabel("  Unscii (Public Domain) by viznut", isSmall = true),
          AboutLabel("  wcwidth (C) 2005-2020, MIT, Rich Felker, et al.", isSmall = true),
          AboutLabel("  JNLua (C) 2008-2012, MIT, Andre Naef", isSmall = true),
          AboutLabel("  LuaJ (C) 2007, MIT, Luaj.org", isSmall = true),
          AboutLabel("  Typesafe Config (C) 2011-2012, Apache License 2.0, Typesafe Inc.", isSmall = true),
          AboutLabel("  LWJGL2 (C) 2002-2007, BSD, Lightweight Java Game Library Project", isSmall = true),
        )

        children :+= new Filler

        children :+= new Widget {
          children :+= new Filler {
            override def maximumSize: Size2D = Size2D(Float.PositiveInfinity, 1)
          }

          if (Desktop.isDesktopSupported) {
            children :+= new PaddingBox(new AboutButton("Website", WebsiteURL), Padding2D(right = 8))
            children :+= new PaddingBox(new AboutButton("IRC", IrcURL), Padding2D(right = 8))
            children :+= new PaddingBox(new AboutButton("Discord", DiscordURL), Padding2D(right = 20))
          }

          children :+= new Button {
            override def text: String = "Ok"
            override def onClick(): Unit = close()
            override protected def colorScheme: ColorScheme = customButtonColorScheme
          }
        }
      },
      Padding2D.equal(18),
    )

    override def draw(g: Graphics): Unit = {
      g.rect(bounds.x, bounds.y, logo.bounds.w, bounds.h, ColorScheme("Accent"))
      super.draw(g)
    }
  }

  override protected def drawInner(g: Graphics): Unit = {
    DrawUtils.shadow(g, bounds.x - 8, bounds.y - 8, bounds.w + 16, bounds.h + 20, 0.5f)
    g.rect(bounds, ColorScheme("ContextMenuBackground"))
    DrawUtils.ring(g, bounds.x, bounds.y, bounds.w, bounds.h, 1, ColorScheme("ContextMenuBorder"))

    drawChildren(g)
  }

  private def customButtonColorScheme = {
    val scheme = new ColorScheme()
    scheme.add("ButtonBackground", ColorScheme("AboutButtonBackground"))
    scheme.add("ButtonBackgroundActive", ColorScheme("AboutButtonBackgroundActive"))
    scheme.add("ButtonBorder", ColorScheme("AboutButtonBorder"))
    scheme.add("ButtonForeground", ColorScheme("AboutButtonForeground"))
    scheme.add("ButtonBackgroundDisabled", ColorScheme("ButtonBackgroundDisabled"))
    scheme.add("ButtonBorderDisabled", ColorScheme("ButtonBorderDisabled"))
    scheme.add("ButtonForegroundDisabled", ColorScheme("ButtonForegroundDisabled"))
    scheme
  }

  private class AboutButton(val label: String, val url: String) extends Button(new LabelTooltip(url)) {
    override def text: String = label
    override def onClick(): Unit = Desktop.getDesktop.browse(new URI(url))
    override protected def colorScheme: ColorScheme = customButtonColorScheme
  }

  private class AboutLabel(private val _text: String, private val _isSmall: Boolean = false) extends Label {
    override def text: String = _text
    override def isSmall: Boolean = _isSmall
    override def color: Color = if (isSmall) ColorScheme("AboutForegroundSubtle") else ColorScheme("AboutForeground")
  }

  private object AboutLabel {
    def apply(text: String, isSmall: Boolean = false): AboutLabel = new AboutLabel(text, isSmall)
  }
}
