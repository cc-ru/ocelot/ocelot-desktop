package ocelot.desktop.ui.widget

trait Updatable {
  def update(): Unit = {}
}
