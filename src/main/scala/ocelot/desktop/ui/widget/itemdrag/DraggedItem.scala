package ocelot.desktop.ui.widget.itemdrag

import ocelot.desktop.geometry.{Size2D, Vector2D}
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.inventory.Item
import ocelot.desktop.ui.widget.Widget
import ocelot.desktop.ui.widget.slot.SlotWidget

case class DraggedItem(item: Item, slot: SlotWidget[_], grabPoint: Vector2D) extends Widget {
  override def minimumSize: Size2D = Size2D(32, 32)
  override def maximumSize: Size2D = minimumSize

  override def draw(g: Graphics): Unit = {
    super.draw(g)

    g.sprite(item.icon, position - grabPoint, minimumSize)
  }
}
