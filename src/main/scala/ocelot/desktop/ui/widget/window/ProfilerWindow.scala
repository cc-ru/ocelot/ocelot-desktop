package ocelot.desktop.ui.widget.window

import ocelot.desktop.graphics.Graphics
import ocelot.desktop.ui.layout.LinearLayout
import ocelot.desktop.ui.widget.{Label, Widget}
import ocelot.desktop.util.{DrawUtils, Orientation, Profiler}

import scala.collection.immutable.ArraySeq

class ProfilerWindow extends PanelWindow {
  override protected def title: String = "Profiler"

  private val inner: Widget = new Widget {
    override val layout = new LinearLayout(this, orientation = Orientation.Vertical)
  }

  setInner(inner)

  override def draw(g: Graphics): Unit = {
    inner.children = ArraySeq.empty
    for (line <- Profiler.report()) {
      inner.children :+= new Label {
        override def text: String = line
      }
    }

    recalculateBounds()
    relayout()

    beginDraw(g)
    DrawUtils.windowWithShadow(g, position.x, position.y, size.width, size.height, 1f, 0.5f)
    drawChildren(g)
    endDraw(g)
  }
}
