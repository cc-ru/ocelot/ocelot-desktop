package ocelot.desktop.ui.widget.window

import ocelot.desktop.ui.layout.{AlignItems, Layout, LinearLayout}
import ocelot.desktop.ui.widget.{IconButton, Label, Widget}
import ocelot.desktop.util.Orientation

class TitleBar(val window: Window) extends Widget {
  def title: String = "Hello world"
  def titleMaxLength: Int = 32
  def isPinned: Boolean = window.isPinned
  def onPinRequested(): Unit = window.pin()
  def onUnpinRequested(): Unit = window.unpin()
  def onCloseRequested(): Unit = window.close()

  override protected val layout: Layout = new LinearLayout(this,
    orientation = Orientation.Horizontal, alignItems = AlignItems.Center)

  children :+= new Label {
    override def text: String = {
      if (title.length > titleMaxLength)
        title.take(titleMaxLength - 1) + "…"
      else
        title
    }

    override def isSmall: Boolean = true
  }

  children :+= new IconButton(
    "icons/Pin",
    "icons/Unpin",
    mode = IconButton.Mode.Switch,
    darkenActiveColorFactor = 0.5f,
    model = IconButton.ReadOnlyModel(isPinned),
  ) {
    override def onPressed(): Unit = onPinRequested()

    override def onReleased(): Unit = onUnpinRequested()
  }

  children :+= new IconButton(
    "icons/Close",
    "icons/Close",
    darkenActiveColorFactor = 0.5f,
  ) {
    override def onPressed(): Unit = onCloseRequested()
  }
}
