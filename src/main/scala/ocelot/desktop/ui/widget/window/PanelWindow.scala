package ocelot.desktop.ui.widget.window

import ocelot.desktop.geometry.Padding2D
import ocelot.desktop.ui.layout.{Layout, LinearLayout}
import ocelot.desktop.ui.widget.{PaddingBox, Widget}
import ocelot.desktop.util.Orientation

import scala.collection.immutable.ArraySeq

trait PanelWindow extends BasicWindow {
  override protected val layout: Layout = new LinearLayout(this, orientation = Orientation.Vertical)

  protected def title: String

  protected def titleMaxLength: Int = 32

  def setInner(inner: Widget, padding: Padding2D = Padding2D(bottom = 13, left = 12, right = 12)): Unit = {
    children = ArraySeq.empty

    children :+= new PaddingBox(
      new TitleBar(PanelWindow.this) {
        override def title: String = PanelWindow.this.title
        override def titleMaxLength: Int = PanelWindow.this.titleMaxLength
      },
      Padding2D(top = 8, left = 12, right = 12, bottom = 2),
    )

    children :+= new PaddingBox(inner, padding)
  }
}
