package ocelot.desktop.ui.widget.settings

import ocelot.desktop.Settings
import ocelot.desktop.geometry.Size2D
import ocelot.desktop.graphics.IconSource
import ocelot.desktop.ui.layout.LinearLayout
import ocelot.desktop.ui.widget.{Checkbox, Slider}
import ocelot.desktop.util.Orientation

class SoundSettingsTab extends SettingsTab {
  override val layout = new LinearLayout(this, orientation = Orientation.Vertical, gap = 8)

  override val icon: IconSource = IconSource.SettingsSound
  override val label: String = "Sound"

  private def addSlider(name: String, value: Float, valueSetter: Float => Unit): Unit = {
    children :+= new Slider(value, name) {
      override def minimumSize: Size2D = Size2D(512, 24)

      override def onValueChanged(value: Float): Unit = {
        valueSetter(value)
        applySettings()
      }
    }
  }

  addSlider(
    "Master volume",
    Settings.get.volumeMaster,
    Settings.get.volumeMaster = _,
  )

  addSlider(
    "Music blocks volume",
    Settings.get.volumeRecords,
    Settings.get.volumeRecords = _,
  )

  addSlider(
    "Environment volume",
    Settings.get.volumeEnvironment,
    Settings.get.volumeEnvironment = _,
  )

  addSlider(
    "Beep volume",
    Settings.get.volumeBeep,
    Settings.get.volumeBeep = _,
  )

  addSlider(
    "Interface volume",
    Settings.get.volumeInterface,
    Settings.get.volumeInterface = _,
  )

  children :+= new Checkbox("Use positional sound", Settings.get.soundPositional) {
    override def minimumSize: Size2D = Size2D(512, 24)

    override def onValueChanged(value: Boolean): Unit = {
      Settings.get.soundPositional = value
      applySettings()
    }
  }
}
