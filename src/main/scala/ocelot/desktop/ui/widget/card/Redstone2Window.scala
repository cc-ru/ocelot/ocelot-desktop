package ocelot.desktop.ui.widget.card

import ocelot.desktop.geometry.{Padding2D, Size2D}
import ocelot.desktop.ui.layout.{AlignItems, Layout, LinearLayout}
import ocelot.desktop.ui.widget.window.PanelWindow
import ocelot.desktop.ui.widget.{Knob, Label, PaddingBox, Widget}
import ocelot.desktop.util.Orientation
import totoro.ocelot.brain.entity.Redstone
import totoro.ocelot.brain.util.{Direction, DyeColor}

class Redstone2Window(card: Redstone.Tier2) extends PanelWindow {
  private def bundledKnob(side: Direction.Value, col: Int) = new Knob(DyeColor.byCode(col)) {
    override def input: Int = {
      card.bundledRedstoneInput(side.id)(col) min 15 max 0
    }

    override def input_=(v: Int): Unit = {
      card.setBundledInput(side, col, v)
    }

    override def output: Int = card.bundledRedstoneOutput(side.id)(col) min 15 max 0
  }

  override protected def title: String = s"Bundled I/O ${card.node.address}"

  override protected def titleMaxLength: Int = 36

  private def bundledBlock(side: Direction.Value, name: String) = new PaddingBox(
    new Widget {
      override protected val layout: Layout = new LinearLayout(this,
        orientation = Orientation.Vertical, alignItems = AlignItems.Center)

      children :+= new Label {
        override def text: String = name

        override def maximumSize: Size2D = minimumSize
      }
      children :+= new Widget {
        children :+= bundledKnob(side, 0)
        children :+= bundledKnob(side, 1)
        children :+= bundledKnob(side, 2)
        children :+= bundledKnob(side, 3)
      }
      children :+= new Widget {
        children :+= bundledKnob(side, 4)
        children :+= bundledKnob(side, 5)
        children :+= bundledKnob(side, 6)
        children :+= bundledKnob(side, 7)
      }
      children :+= new Widget {
        children :+= bundledKnob(side, 8)
        children :+= bundledKnob(side, 9)
        children :+= bundledKnob(side, 10)
        children :+= bundledKnob(side, 11)
      }
      children :+= new Widget {
        children :+= bundledKnob(side, 12)
        children :+= bundledKnob(side, 13)
        children :+= bundledKnob(side, 14)
        children :+= bundledKnob(side, 15)
      }
    },
    Padding2D.equal(4),
  )

  setInner(new Widget {
    children :+= new Widget {
      override protected val layout: Layout = new LinearLayout(this, orientation = Orientation.Vertical)
      children :+= bundledBlock(Direction.Down, "Bottom")
      children :+= bundledBlock(Direction.Up, "Top")
    }
    children :+= new Widget {
      override protected val layout: Layout = new LinearLayout(this, orientation = Orientation.Vertical)
      children :+= bundledBlock(Direction.Back, "Back")
      children :+= bundledBlock(Direction.Front, "Front")
    }
    children :+= new Widget {
      override protected val layout: Layout = new LinearLayout(this, orientation = Orientation.Vertical)
      children :+= bundledBlock(Direction.Right, "Right")
      children :+= bundledBlock(Direction.Left, "Left")
    }
  })
}
