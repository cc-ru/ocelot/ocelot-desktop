package ocelot.desktop.ui.widget.card

import ocelot.desktop.geometry.{Padding2D, Size2D}
import ocelot.desktop.ui.layout.{AlignItems, Layout, LinearLayout}
import ocelot.desktop.ui.widget.window.PanelWindow
import ocelot.desktop.ui.widget.{Knob, Label, PaddingBox, Widget}
import ocelot.desktop.util.Orientation
import totoro.ocelot.brain.entity.Redstone
import totoro.ocelot.brain.util.{Direction, DyeColor}

class Redstone1Window(card: Redstone.Tier1) extends PanelWindow {
  private def redstoneKnob(side: Direction.Value) = new Knob(DyeColor.Red) {
    override def input: Int = {
      card.redstoneInput(side.id) min 15 max 0
    }

    override def input_=(v: Int): Unit = {
      card.setRedstoneInput(side, v)
    }

    override def output: Int = card.redstoneOutput(side.id) min 15 max 0
  }

  override protected def title: String = s"Redstone I/O ${card.node.address}"

  override protected def titleMaxLength: Int = 26

  private def redstoneBlock(side: Direction.Value, name: String) = new PaddingBox(
    new Widget {
      override protected val layout: Layout =
        new LinearLayout(this, Orientation.Horizontal, alignItems = AlignItems.Center)

      children :+= new PaddingBox(
        new Label {
          override def text: String = name

          override def maximumSize: Size2D = minimumSize
        },
        Padding2D(right = 8),
      )

      children :+= redstoneKnob(side)
    },
    Padding2D.equal(4),
  )

  setInner(new Widget {
    children :+= new Widget {
      override protected val layout: Layout = new LinearLayout(this, orientation = Orientation.Vertical)
      children :+= redstoneBlock(Direction.Down, "Bottom")
      children :+= redstoneBlock(Direction.Up, "   Top")
    }
    children :+= new Widget {
      override protected val layout: Layout = new LinearLayout(this, orientation = Orientation.Vertical)
      children :+= redstoneBlock(Direction.Back, " Back")
      children :+= redstoneBlock(Direction.Front, "Front")
    }
    children :+= new Widget {
      override protected val layout: Layout = new LinearLayout(this, orientation = Orientation.Vertical)
      children :+= redstoneBlock(Direction.Right, "Right")
      children :+= redstoneBlock(Direction.Left, " Left")
    }
  })
}
