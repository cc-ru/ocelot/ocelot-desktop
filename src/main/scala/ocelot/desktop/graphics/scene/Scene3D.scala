package ocelot.desktop.graphics.scene

import ocelot.desktop.color.Color

import scala.collection.mutable.ArrayBuffer

class Scene3D {
  var camera: Camera3D = new Camera3D
  var lightSource: DirectionalLight3D = new DirectionalLight3D
  var ambientLightEnergy: Float = 0.3f
  var ambientLightColor: Color = Color.White
  val meshes: ArrayBuffer[SceneMesh3D] = new ArrayBuffer[SceneMesh3D]()
}
