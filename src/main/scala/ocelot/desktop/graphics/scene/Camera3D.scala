package ocelot.desktop.graphics.scene

import ocelot.desktop.geometry.{ProjectionMatrix3D, Vector3D}

class Camera3D(var zNear: Float = 0.1f,
               var zFar: Float = 100.0f,
               var fovY: Float = 80.0f)
    extends SceneNode3D {
  def distanceTo(point: Vector3D): Float = {
    (origin - point).length
  }

  def projectionMatrix(aspect: Float): ProjectionMatrix3D = ProjectionMatrix3D.perspective(aspect, fovY, zNear, zFar)
}
