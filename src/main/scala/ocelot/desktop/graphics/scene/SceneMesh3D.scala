package ocelot.desktop.graphics.scene

import ocelot.desktop.color.Color
import ocelot.desktop.geometry.Transform2D
import ocelot.desktop.graphics.mesh.Mesh3D

class SceneMesh3D(var mesh: Mesh3D,
                  var color: Color = Color.White,
                  var uvTransform: Transform2D = Transform2D.identity,
                  var lightingFactor: Float = 1,
                  var textureFactor: Float = 1,
                  var isOpaque: Boolean = true,
                  var priority: Int = 0)
    extends SceneNode3D
