package ocelot.desktop.graphics.scene

import ocelot.desktop.geometry.{Basis3D, Transform3D, Vector3D}

trait SceneNode3D {
  var transform: Transform3D = Transform3D.identity

  final def origin: Vector3D = transform.origin

  final def origin_=(new_origin: Vector3D): Unit = transform = Transform3D(basis, new_origin)

  final def basis: Basis3D = transform.basis

  final def basis_=(new_basis: Basis3D): Unit = transform = Transform3D(new_basis, origin)
}
