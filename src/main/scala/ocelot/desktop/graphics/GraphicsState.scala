package ocelot.desktop.graphics

import ocelot.desktop.color.RGBAColorNorm
import ocelot.desktop.geometry.Transform2D

case class GraphicsState(
  var foreground: RGBAColorNorm = RGBAColorNorm(0f, 0f, 0f),
  var background: RGBAColorNorm = RGBAColorNorm(1f, 1f, 1f),
  var fontSizeMultiplier: Float = 1f,
  var alphaMultiplier: Float = 1f,
  var sprite: String = "Empty",
  var scissor: Option[(Float, Float, Float, Float)] = None,
  var transform: Transform2D = Transform2D.identity,
)
