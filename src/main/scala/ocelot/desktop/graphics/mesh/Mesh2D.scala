package ocelot.desktop.graphics.mesh

import ocelot.desktop.geometry.Vector2D
import ocelot.desktop.graphics.buffer.Index

object Mesh2D {
  val quad: Mesh2D = new Mesh2D(
    Array(Vector2D(0f, 0f), Vector2D(1f, 0f), Vector2D(1f, 1f),
      Vector2D(1f, 1f), Vector2D(0f, 1f), Vector2D(0f, 0f)).map(v => MeshVertex2D(v, v))
  )
}

class Mesh2D(override val vertices: Seq[MeshVertex2D], override val indices: Option[Seq[Index]] = None)
    extends Mesh[MeshVertex2D]
