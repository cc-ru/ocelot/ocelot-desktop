package ocelot.desktop.graphics.mesh

import ocelot.desktop.color.RGBAColorNorm
import ocelot.desktop.geometry.{Vector2D, Vector3D}
import org.lwjgl.opengl.GL11

import java.nio.ByteBuffer

object MeshVertex3D extends VertexType {
  override val stride: Int = 48

  override val attributes: Seq[Attribute] = Array(
    Attribute("inPos", 3, GL11.GL_FLOAT, normalized = false, stride, 0),
    Attribute("inNormal", 3, GL11.GL_FLOAT, normalized = false, stride, 12),
    Attribute("inUV", 2, GL11.GL_FLOAT, normalized = false, stride, 24),
    Attribute("inColor", 4, GL11.GL_FLOAT, normalized = false, stride, 32),
  )
}

case class MeshVertex3D(pos: Vector3D, normal: Vector3D, uv: Vector2D, color: RGBAColorNorm) extends Vertex {
  override def stride: Int = MeshVertex3D.stride

  override def vertexType: VertexType = MeshVertex3D

  override def put(buf: ByteBuffer): Unit = {
    buf.putFloat(pos.x)
    buf.putFloat(pos.y)
    buf.putFloat(pos.z)
    buf.putFloat(normal.x)
    buf.putFloat(normal.y)
    buf.putFloat(normal.z)
    buf.putFloat(uv.x)
    buf.putFloat(uv.y)
    buf.putFloat(color.r)
    buf.putFloat(color.g)
    buf.putFloat(color.b)
    buf.putFloat(color.a)
  }
}
