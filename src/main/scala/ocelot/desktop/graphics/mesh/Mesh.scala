package ocelot.desktop.graphics.mesh

import ocelot.desktop.graphics.buffer.Index

trait Mesh[V <: Vertex] {
  def vertices: Seq[V]

  def indices: Option[Seq[Index]]

  def primitiveType: PrimitiveType = PrimitiveTriangles
}
