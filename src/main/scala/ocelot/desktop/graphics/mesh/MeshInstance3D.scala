package ocelot.desktop.graphics.mesh

import ocelot.desktop.color.Color
import ocelot.desktop.geometry.{Transform2D, Transform3D}
import org.lwjgl.opengl.GL11

import java.nio.ByteBuffer

object MeshInstance3D extends VertexType {
  override val stride: Int = 96

  override val attributes: Seq[Attribute] = Array(
    Attribute("inInstanceColor", 4, GL11.GL_FLOAT, normalized = false, stride, 0),
    Attribute("inTransform0", 4, GL11.GL_FLOAT, normalized = false, stride, 16),
    Attribute("inTransform1", 4, GL11.GL_FLOAT, normalized = false, stride, 32),
    Attribute("inTransform2", 4, GL11.GL_FLOAT, normalized = false, stride, 48),
    Attribute("inUVTransform0", 3, GL11.GL_FLOAT, normalized = false, stride, 64),
    Attribute("inUVTransform1", 3, GL11.GL_FLOAT, normalized = false, stride, 76),
    Attribute("inLightingFactor", 1, GL11.GL_FLOAT, normalized = false, stride, 88),
    Attribute("inTextureFactor", 1, GL11.GL_FLOAT, normalized = false, stride, 92),
  )
}

case class MeshInstance3D(color: Color, transform: Transform3D, uvTransform: Transform2D,
                          lightingFactor: Float, textureFactor: Float)
    extends Vertex {
  override def vertexType: VertexType = MeshInstance3D

  override def stride: Int = MeshInstance3D.stride

  override def put(buf: ByteBuffer): Unit = {
    color.toRGBANorm.put(buf)
    transform.put(buf)
    uvTransform.put(buf)
    buf.putFloat(lightingFactor)
    buf.putFloat(textureFactor)
  }
}
