package ocelot.desktop.graphics.mesh

import org.lwjgl.opengl.GL11

sealed trait PrimitiveType {
  def toGL: Int
}

object PrimitiveTriangles extends PrimitiveType {
  override def toGL: Int = GL11.GL_TRIANGLES
}

object PrimitiveLines extends PrimitiveType {
  override def toGL: Int = GL11.GL_LINES
}
