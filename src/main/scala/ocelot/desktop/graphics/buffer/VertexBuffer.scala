package ocelot.desktop.graphics.buffer

import ocelot.desktop.graphics.mesh.{Vertex, VertexType}

class VertexBuffer[V <: Vertex] extends Buffer[V] {
  var ty: VertexType = _

  def this(elements: Seq[V]) = {
    this()
    createStatic(elements)
  }

  def this(vertexType: VertexType, cap: Int) = {
    this()

    ty = vertexType
    createDynamic(ty.stride, cap)
  }

  override protected def extractMeta(head: V): Unit = {
    ty = head.vertexType
    super.extractMeta(head)
  }
}
