package ocelot.desktop.graphics.buffer

import java.nio.ByteBuffer

trait BufferPut {
  def stride: Int

  def put(buf: ByteBuffer): Unit
}
