package ocelot.desktop.inventory.item

import ocelot.desktop.graphics.IconSource
import ocelot.desktop.inventory.traits.{CardItem, ComponentItem, PersistableItem}
import ocelot.desktop.inventory.{Item, ItemFactory, ItemRecoverer}
import ocelot.desktop.ui.widget.tooltip.ItemTooltip
import totoro.ocelot.brain.Settings
import totoro.ocelot.brain.entity.DataCard
import totoro.ocelot.brain.entity.traits.{Entity, Environment}
import totoro.ocelot.brain.util.Tier
import totoro.ocelot.brain.util.Tier.Tier

abstract class DataCardItem extends Item with ComponentItem with PersistableItem with CardItem {
  override def fillTooltip(tooltip: ItemTooltip): Unit = {
    super.fillTooltip(tooltip)
    tooltip.addLine(
      s"Soft limit (${Settings.get.dataCardTimeout} sec slowdown): ${Settings.get.dataCardSoftLimit} bytes"
    )
    tooltip.addLine(s"Hard limit (fail): ${Settings.get.dataCardHardLimit} bytes")
  }
}

object DataCardItem {
  abstract class Factory extends ItemFactory {
    override def name: String = s"Data Card (${tier.get.label})"

    override def icon: IconSource = IconSource.Items.DataCard(tier.get)
  }

  class Tier1(val dataCard: DataCard.Tier1) extends DataCardItem {
    override def entity: Entity with Environment = dataCard

    override def factory: Factory = DataCardItem.Tier1.Factory
  }

  object Tier1 {
    object Factory extends Factory {
      override type I = DataCardItem.Tier1

      override def itemClass: Class[I] = classOf

      override def tier: Option[Tier] = Some(Tier.One)

      override def build(): DataCardItem.Tier1 = new DataCardItem.Tier1(new DataCard.Tier1)

      override def recoverers: Iterable[ItemRecoverer[_, _]] = Some(ItemRecoverer(new DataCardItem.Tier1(_)))
    }
  }

  class Tier2(val dataCard: DataCard.Tier2) extends DataCardItem {
    override def entity: Entity with Environment = dataCard

    override def factory: Factory = DataCardItem.Tier2.Factory
  }

  object Tier2 {
    object Factory extends Factory {
      override type I = DataCardItem.Tier2

      override def itemClass: Class[I] = classOf

      override def tier: Option[Tier] = Some(Tier.Two)

      override def build(): DataCardItem.Tier2 = new DataCardItem.Tier2(new DataCard.Tier2)

      override def recoverers: Iterable[ItemRecoverer[_, _]] = Some(ItemRecoverer(new DataCardItem.Tier2(_)))
    }
  }

  class Tier3(val dataCard: DataCard.Tier3) extends DataCardItem {
    override def entity: Entity with Environment = dataCard

    override def factory: Factory = DataCardItem.Tier3.Factory
  }

  object Tier3 {
    object Factory extends Factory {
      override type I = DataCardItem.Tier3

      override def itemClass: Class[I] = classOf

      override def tier: Option[Tier] = Some(Tier.Three)

      override def build(): DataCardItem.Tier3 = new DataCardItem.Tier3(new DataCard.Tier3)

      override def recoverers: Iterable[ItemRecoverer[_, _]] = Some(ItemRecoverer(new DataCardItem.Tier3(_)))
    }
  }
}
