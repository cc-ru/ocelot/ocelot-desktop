package ocelot.desktop.inventory.item

import ocelot.desktop.graphics.IconSource
import ocelot.desktop.inventory.traits.{CardItem, ComponentItem, PersistableItem}
import ocelot.desktop.inventory.{Item, ItemFactory, ItemRecoverer}
import ocelot.desktop.ui.widget.tooltip.ItemTooltip
import totoro.ocelot.brain.entity.NetworkCard
import totoro.ocelot.brain.entity.traits.Entity
import totoro.ocelot.brain.util.Tier
import totoro.ocelot.brain.util.Tier.Tier

class NetworkCardItem(val card: NetworkCard) extends Item with ComponentItem with PersistableItem with CardItem {
  override def entity: Entity with NetworkCard = card

  override def factory: ItemFactory = NetworkCardItem.Factory

  override def fillTooltip(tooltip: ItemTooltip): Unit = {
    super.fillTooltip(tooltip)
    if (card != null) {
      tooltip.addLine(
        if (card.openPorts.isEmpty) s"Open ports: none"
        else s"Open ports: ${card.openPorts.mkString(", ")}"
      )
      tooltip.addLine(s"Max open ports: ${card.maxOpenPorts}")
    }
  }
}

object NetworkCardItem {
  object Factory extends ItemFactory {
    override type I = NetworkCardItem

    override def itemClass: Class[I] = classOf

    override def name: String = "Network Card"
    override def tier: Option[Tier] = Some(Tier.One)
    override def icon: IconSource = IconSource.Items.NetworkCard

    override def build(): NetworkCardItem = new NetworkCardItem(new NetworkCard)

    override def recoverers: Iterable[ItemRecoverer[_, _]] = Some(ItemRecoverer(new NetworkCardItem(_)))
  }
}
