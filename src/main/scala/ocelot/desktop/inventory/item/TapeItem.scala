package ocelot.desktop.inventory.item

import ocelot.desktop.OcelotDesktop
import ocelot.desktop.graphics.IconSource
import ocelot.desktop.inventory.traits.EntityItem
import ocelot.desktop.inventory.{Item, ItemFactory, ItemRecoverer}
import ocelot.desktop.ui.widget.tooltip.ItemTooltip
import ocelot.desktop.util.SizeFormatting.formatSize
import totoro.ocelot.brain.entity.tape.Tape
import totoro.ocelot.brain.nbt.NBTTagCompound
import totoro.ocelot.brain.util.Tier.Tier

class TapeItem(var tape: Tape) extends Item with EntityItem {
  def this() = this(new Tape)

  override def entity: Tape = tape

  override def fillTooltip(tooltip: ItemTooltip): Unit = {
    super.fillTooltip(tooltip)

    if (tape.label.nonEmpty) {
      tooltip.addLine(s"Label: ${tape.label}")
    }

    for (storageId <- tape.storageId) {
      tooltip.addLine(s"ID: $storageId")
    }

    tooltip.addLine(s"Size: ${formatSize(tape.size)}")
    tooltip.addLine(f"Length: ${tape.lengthMinutes}%.0f minutes")
  }

  override def load(nbt: NBTTagCompound): Unit = {
    super.load(nbt)
    tape.load(nbt, OcelotDesktop.workspace)
  }

  override def save(nbt: NBTTagCompound): Unit = {
    super.save(nbt)
    tape.save(nbt)
  }

  override def factory: ItemFactory = new TapeItem.Factory(tape.kind)
}

object TapeItem {
  class Factory(val kind: Tape.Kind) extends ItemFactory {
    override type I = TapeItem

    override def itemClass: Class[TapeItem] = classOf

    override def name: String = "Tape"

    override def tier: Option[Tier] = None

    override def icon: IconSource = IconSource.Items.Tape(kind)

    override def build(): TapeItem = new TapeItem(new Tape(kind))

    override def recoverers: Iterable[ItemRecoverer[_, _]] = Some(ItemRecoverer[Tape, TapeItem](new TapeItem(_)))
  }
}
