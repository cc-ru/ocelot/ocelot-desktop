package ocelot.desktop.inventory.item

import ocelot.desktop.color.Color
import ocelot.desktop.graphics.IconSource
import ocelot.desktop.inventory.{ItemFactory, ItemRecoverer}
import ocelot.desktop.util.TierColor
import totoro.ocelot.brain.entity.MagicalMemory
import totoro.ocelot.brain.util.Tier.Tier
import totoro.ocelot.brain.util.{ExtendedTier, Tier}

class MagicalMemoryItem(override val memory: MagicalMemory) extends MemoryItem(memory) {
  override def factory: ItemFactory = new MagicalMemoryItem.Factory()

  override def tooltipNameColor: Color = TierColor.Tier3
}

object MagicalMemoryItem {
  class Factory() extends ItemFactory {
    override type I = MagicalMemoryItem

    override def itemClass: Class[I] = classOf

    override def name: String = "Magical Memory (Creative)"

    override def tier: Option[Tier] = Some(Tier.One)

    override def icon: IconSource = IconSource.Items.Memory(ExtendedTier.Creative)

    override def build(): MagicalMemoryItem = new MagicalMemoryItem(new MagicalMemory())

    override def recoverers: Iterable[ItemRecoverer[_, _]] = Some(ItemRecoverer(new MagicalMemoryItem(_)))
  }
}
