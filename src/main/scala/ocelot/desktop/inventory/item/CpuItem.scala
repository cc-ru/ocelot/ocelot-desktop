package ocelot.desktop.inventory.item

import ocelot.desktop.graphics.IconSource
import ocelot.desktop.inventory.traits.{ComponentItem, CpuLikeItem, PersistableItem}
import ocelot.desktop.inventory.{Item, ItemFactory, ItemRecoverer}
import totoro.ocelot.brain.entity.CPU
import totoro.ocelot.brain.entity.traits.{Entity, GenericCPU}
import totoro.ocelot.brain.util.Tier.Tier

class CpuItem(val cpu: CPU) extends Item with ComponentItem with PersistableItem with CpuLikeItem {
  override def entity: Entity with GenericCPU = cpu

  override def factory: CpuItem.Factory = new CpuItem.Factory(cpu.tier)
}

object CpuItem {
  class Factory(_tier: Tier) extends ItemFactory {
    override type I = CpuItem

    override def itemClass: Class[I] = classOf

    override def name: String = s"CPU (${_tier.label})"

    override def tier: Option[Tier] = Some(_tier)

    override def icon: IconSource = IconSource.Items.Cpu(_tier)

    override def build(): CpuItem = new CpuItem(new CPU(_tier))

    override def recoverers: Iterable[ItemRecoverer[_, _]] = Some(ItemRecoverer(new CpuItem(_)))
  }
}
