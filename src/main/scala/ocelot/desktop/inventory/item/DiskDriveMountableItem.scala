package ocelot.desktop.inventory.item

import ocelot.desktop.graphics.IconSource
import ocelot.desktop.inventory.traits.RackMountableItem
import ocelot.desktop.inventory.{ItemFactory, ItemRecoverer}
import ocelot.desktop.ui.widget.tooltip.ItemTooltip
import ocelot.desktop.util.DiskDriveAware
import totoro.ocelot.brain.entity.traits.Floppy
import totoro.ocelot.brain.entity.{DiskDriveMountable, FloppyDiskDrive}
import totoro.ocelot.brain.util.Tier.Tier

class DiskDriveMountableItem(val diskDriveMountable: DiskDriveMountable) extends RackMountableItem with DiskDriveAware {
  override def floppyDiskDrive: FloppyDiskDrive = diskDriveMountable

  override def entity: DiskDriveMountable = diskDriveMountable
  override def factory: ItemFactory = DiskDriveMountableItem.Factory

  override def fillTooltip(tooltip: ItemTooltip): Unit = {
    super.fillTooltip(tooltip)
    if (diskDriveMountable != null) {
      diskDriveMountable.inventory(0).get match {
        case None => tooltip.addLine("Floppy: none")
        case Some(floppy: Floppy) if floppy.name.isDefined => tooltip.addLine(s"Floppy: ${floppy.name.get}")
        case _ =>
      }
    }
  }
}

object DiskDriveMountableItem {
  object Factory extends ItemFactory {
    override type I = DiskDriveMountableItem

    override def itemClass: Class[I] = classOf

    override def tier: Option[Tier] = None

    override def name: String = "Disk Drive"

    override def icon: IconSource = IconSource.Items.DiskDriveMountable

    override def build(): DiskDriveMountableItem = {
      val item = new DiskDriveMountableItem(new DiskDriveMountable())
      item.fillSlotsWithDefaultItems()

      item
    }

    override def recoverers: Iterable[ItemRecoverer[_, _]] = Some(ItemRecoverer(new DiskDriveMountableItem(_)))
  }
}
