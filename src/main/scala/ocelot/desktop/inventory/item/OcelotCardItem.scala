package ocelot.desktop.inventory.item

import ocelot.desktop.ColorScheme
import ocelot.desktop.color.Color
import ocelot.desktop.entity.OcelotCard
import ocelot.desktop.entity.traits.OcelotInterface
import ocelot.desktop.graphics.IconSource
import ocelot.desktop.inventory.traits.{CardItem, ComponentItem, PersistableItem}
import ocelot.desktop.inventory.{Item, ItemFactory, ItemRecoverer}
import ocelot.desktop.ui.widget.contextmenu.{ContextMenu, ContextMenuEntry}
import ocelot.desktop.ui.widget.tooltip.ItemTooltip
import ocelot.desktop.util.{Logging, OcelotInterfaceLogStorage}
import totoro.ocelot.brain.util.Tier
import totoro.ocelot.brain.util.Tier.Tier

class OcelotCardItem(val ocelotCard: OcelotCard)
    extends Item with ComponentItem with OcelotInterfaceLogStorage with PersistableItem with CardItem with Logging {

  override def entity: OcelotCard = ocelotCard

  override def ocelotInterface: OcelotInterface = ocelotCard

  override def tooltipNameColor: Color = ColorScheme("OcelotCardTooltip")

  override def fillRmbMenu(menu: ContextMenu): Unit = {
    menu.addEntry(ContextMenuEntry("Open console", IconSource.Window) {
      window.open()
    })

    menu.addSeparator()

    super.fillRmbMenu(menu)
  }

  override def factory: ItemFactory = OcelotCardItem.Factory

  private val OcelotSays = Array("meow", ":3", "♥", "meooow", "~(=^–^)", "/ᐠ｡ꞈ｡ᐟ\\", "=^._.^=", "=’①。①’=")

  override def fillTooltip(tooltip: ItemTooltip): Unit = {
    super.fillTooltip(tooltip)
    tooltip.addLine(OcelotSays(((System.currentTimeMillis() / 5000) % OcelotSays.length).toInt))
  }
}

object OcelotCardItem {
  object Factory extends ItemFactory {
    override type I = OcelotCardItem

    override def itemClass: Class[I] = classOf

    override def name: String = "Ocelot Card"

    override def tier: Option[Tier] = Some(Tier.One)

    override def icon: IconSource = IconSource.Items.OcelotCard

    override def build(): OcelotCardItem = new OcelotCardItem(new OcelotCard)

    override def recoverers: Iterable[ItemRecoverer[_, _]] = Some(ItemRecoverer(new OcelotCardItem(_)))
  }
}
