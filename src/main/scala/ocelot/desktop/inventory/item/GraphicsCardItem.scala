package ocelot.desktop.inventory.item

import ocelot.desktop.graphics.IconSource
import ocelot.desktop.inventory.traits.{CardItem, ComponentItem, GpuLikeItem, PersistableItem}
import ocelot.desktop.inventory.{Item, ItemFactory, ItemRecoverer}
import totoro.ocelot.brain.entity.GraphicsCard
import totoro.ocelot.brain.entity.traits.{Entity, GenericGPU}
import totoro.ocelot.brain.util.Tier.Tier

class GraphicsCardItem(val gpu: GraphicsCard)
    extends Item with ComponentItem with PersistableItem with CardItem with GpuLikeItem {
  override def entity: Entity with GenericGPU = gpu

  override def factory: GraphicsCardItem.Factory = new GraphicsCardItem.Factory(gpu.tier)
}

object GraphicsCardItem {
  class Factory(_tier: Tier) extends ItemFactory {
    override type I = GraphicsCardItem

    override def itemClass: Class[I] = classOf

    override def name: String = s"Graphics Card (${_tier.label})"
    override def tier: Option[Tier] = Some(_tier)
    override def icon: IconSource = IconSource.Items.GraphicsCard(_tier)

    override def build(): GraphicsCardItem = new GraphicsCardItem(new GraphicsCard(_tier))

    override def recoverers: Iterable[ItemRecoverer[_, _]] = Some(ItemRecoverer(new GraphicsCardItem(_)))
  }
}
