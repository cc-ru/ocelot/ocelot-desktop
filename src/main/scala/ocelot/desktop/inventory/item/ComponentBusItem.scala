package ocelot.desktop.inventory.item

import ocelot.desktop.graphics.IconSource
import ocelot.desktop.inventory.traits.EntityItem
import ocelot.desktop.inventory.{Item, ItemFactory, ItemRecoverer}
import ocelot.desktop.ui.widget.tooltip.ItemTooltip
import totoro.ocelot.brain.entity.ComponentBus
import totoro.ocelot.brain.util.Tier.Tier

class ComponentBusItem(val componentBus: ComponentBus) extends Item with EntityItem {
  override def entity: ComponentBus = componentBus
  override def factory: ItemFactory = new ComponentBusItem.Factory(componentBus.tier)

  override def fillTooltip(tooltip: ItemTooltip): Unit = {
    super.fillTooltip(tooltip)
    if (componentBus != null) {
      tooltip.addLine(s"Components: ${componentBus.supportedComponents}")
    }
  }
}

object ComponentBusItem {
  class Factory(_tier: Tier) extends ItemFactory {
    override type I = ComponentBusItem

    override def itemClass: Class[I] = classOf

    override def name: String = s"Component Bus (${_tier.label})"

    override def tier: Option[Tier] = Some(_tier)

    override def icon: IconSource = IconSource.Items.ComponentBus(_tier)

    override def build(): ComponentBusItem = new ComponentBusItem(new ComponentBus(_tier))

    override def recoverers: Iterable[ItemRecoverer[_, _]] = Some(ItemRecoverer(new ComponentBusItem(_)))
  }
}
