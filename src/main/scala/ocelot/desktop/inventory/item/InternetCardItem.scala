package ocelot.desktop.inventory.item

import ocelot.desktop.graphics.IconSource
import ocelot.desktop.inventory.traits.{CardItem, ComponentItem, PersistableItem}
import ocelot.desktop.inventory.{Item, ItemFactory, ItemRecoverer}
import totoro.ocelot.brain.entity.InternetCard
import totoro.ocelot.brain.entity.traits.{Entity, Environment}
import totoro.ocelot.brain.util.Tier
import totoro.ocelot.brain.util.Tier.Tier

class InternetCardItem(val card: InternetCard) extends Item with ComponentItem with PersistableItem with CardItem {
  override def entity: Entity with Environment = card

  override def factory: ItemFactory = InternetCardItem.Factory
}

object InternetCardItem {
  object Factory extends ItemFactory {
    override type I = InternetCardItem

    override def itemClass: Class[InternetCardItem] = classOf

    override def name: String = "Internet Card"

    override def tier: Option[Tier] = Some(Tier.Two)

    override def icon: IconSource = IconSource.Items.InternetCard

    override def build(): InternetCardItem = new InternetCardItem(new InternetCard)

    override def recoverers: Iterable[ItemRecoverer[_, _]] = Some(ItemRecoverer(new InternetCardItem(_)))
  }
}
