package ocelot.desktop.inventory.item

import ocelot.desktop.graphics.IconSource
import ocelot.desktop.inventory.traits.{CardItem, ComponentItem, PersistableItem}
import ocelot.desktop.inventory.{Item, ItemFactory, ItemRecoverer}
import ocelot.desktop.ui.widget.TunnelDialog
import ocelot.desktop.ui.widget.contextmenu.{ContextMenu, ContextMenuEntry}
import ocelot.desktop.ui.widget.tooltip.ItemTooltip
import totoro.ocelot.brain.entity.LinkedCard
import totoro.ocelot.brain.entity.traits.{Entity, Environment}
import totoro.ocelot.brain.util.Tier
import totoro.ocelot.brain.util.Tier.Tier

class LinkedCardItem(val linkedCard: LinkedCard) extends Item with ComponentItem with PersistableItem with CardItem {
  override def entity: Entity with Environment = linkedCard

  override def fillTooltip(tooltip: ItemTooltip): Unit = {
    super.fillTooltip(tooltip)

    tooltip.addLine(s"Channel: ${linkedCard.tunnel}")
  }

  override def fillRmbMenu(menu: ContextMenu): Unit = {
    menu.addEntry(
      ContextMenuEntry("Set channel", IconSource.Antenna) {
        new TunnelDialog(
          tunnel => linkedCard.tunnel = tunnel,
          linkedCard.tunnel,
        ).show()
      }
    )
    super.fillRmbMenu(menu)
  }

  override def factory: ItemFactory = LinkedCardItem.Factory
}

object LinkedCardItem {
  object Factory extends ItemFactory {
    override type I = LinkedCardItem

    override def itemClass: Class[I] = classOf

    override def name: String = "Linked Card"

    override def tier: Option[Tier] = Some(Tier.Three)

    override def icon: IconSource = IconSource.Items.LinkedCard

    override def build(): LinkedCardItem = new LinkedCardItem(new LinkedCard)

    override def recoverers: Iterable[ItemRecoverer[_, _]] = Some(ItemRecoverer(new LinkedCardItem(_)))
  }
}
