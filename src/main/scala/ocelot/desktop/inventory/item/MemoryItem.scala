package ocelot.desktop.inventory.item

import ocelot.desktop.graphics.IconSource
import ocelot.desktop.inventory.traits.{ComponentItem, PersistableItem}
import ocelot.desktop.inventory.{Item, ItemFactory, ItemRecoverer}
import ocelot.desktop.ui.widget.tooltip.ItemTooltip
import totoro.ocelot.brain.entity.Memory
import totoro.ocelot.brain.entity.traits.{Entity, Environment}
import totoro.ocelot.brain.util.ExtendedTier.ExtendedTier
import totoro.ocelot.brain.util.Tier.Tier

class MemoryItem(val memory: Memory) extends Item with ComponentItem with PersistableItem {
  override def entity: Entity with Environment = memory

  override def factory: ItemFactory = new MemoryItem.Factory(memory.memoryTier)

  override def fillTooltip(tooltip: ItemTooltip): Unit = {
    super.fillTooltip(tooltip)
    tooltip.addLine(
      if (memory.amount == Double.PositiveInfinity) "Capacity: infinite"
      else s"Capacity: ${memory.amount.toInt} kB"
    )
  }
}

object MemoryItem {
  class Factory(memoryTier: ExtendedTier) extends ItemFactory {
    override type I = MemoryItem

    override def itemClass: Class[I] = classOf

    override def name: String = s"Memory (${memoryTier.label})"

    override def tier: Option[Tier] = Some(memoryTier.toTier)

    override def icon: IconSource = IconSource.Items.Memory(memoryTier)

    override def build(): MemoryItem = new MemoryItem(new Memory(memoryTier))

    override def recoverers: Iterable[ItemRecoverer[_, _]] = Some(ItemRecoverer(new MemoryItem(_)))
  }
}
