package ocelot.desktop.inventory.item

import ocelot.desktop.graphics.IconSource
import ocelot.desktop.inventory.traits.RackMountableItem
import ocelot.desktop.inventory.{ItemFactory, ItemRecoverer}
import ocelot.desktop.ui.widget.slot._
import ocelot.desktop.util.ComputerType.ComputerType
import ocelot.desktop.util.{AudibleComputerAware, ComputerType}
import totoro.ocelot.brain.entity.Server
import totoro.ocelot.brain.entity.traits.Inventory
import totoro.ocelot.brain.util.Tier
import totoro.ocelot.brain.util.Tier.Tier

class ServerItem(val server: Server) extends RackMountableItem with AudibleComputerAware {
  override def entity: Server = server
  override def factory: ItemFactory = new ServerItem.Factory(server.tier)

  override def onRemoved(): Unit = {
    turnOff()
    closeWindow()

    super.onRemoved()
  }

  // ---------------------------- ComputerAware ----------------------------

  override def computer: Server = server
  override def computerType: ComputerType = ComputerType.Server
  override def brainInventory: Inventory = server.inventory.owner

  override def addSlotsBasedOnTier(): Unit = {
    floppySlot = None

    server.tier match {
      case Tier.One =>
        cardSlots = addSlotWidgets(new CardSlotWidget(_, Tier.Two), new CardSlotWidget(_, Tier.Two))
        cpuSlot = addSlotWidget(new ComputerCpuSlotWidget(_, this, Tier.Two))
        componentBusSlots = addSlotWidgets(new ComponentBusSlotWidget(_, Tier.Two))
        memorySlots = addSlotWidgets(new MemorySlotWidget(_, Tier.Two), new MemorySlotWidget(_, Tier.Two))
        diskSlots = addSlotWidgets(new HddSlotWidget(_, Tier.Two))
        eepromSlot = addSlotWidget(new EepromSlotWidget(_))

      case Tier.Two =>
        cardSlots = addSlotWidgets(new CardSlotWidget(_, Tier.Three), new CardSlotWidget(_, Tier.Two),
          new CardSlotWidget(_, Tier.Two))
        cpuSlot = addSlotWidget(new ComputerCpuSlotWidget(_, this, Tier.Three))
        componentBusSlots =
          addSlotWidgets(new ComponentBusSlotWidget(_, Tier.Three), new ComponentBusSlotWidget(_, Tier.Three))
        memorySlots = addSlotWidgets(new MemorySlotWidget(_, Tier.Three), new MemorySlotWidget(_, Tier.Three),
          new MemorySlotWidget(_, Tier.Three))
        diskSlots = addSlotWidgets(new HddSlotWidget(_, Tier.Three), new HddSlotWidget(_, Tier.Three))
        eepromSlot = addSlotWidget(new EepromSlotWidget(_))

      case _ =>
        cardSlots = {
          if (server.tier == Tier.Three) {
            addSlotWidgets(
              new CardSlotWidget(_, Tier.Three),
              new CardSlotWidget(_, Tier.Three),
              new CardSlotWidget(_, Tier.Two),
              new CardSlotWidget(_, Tier.Two),
            )
          } else {
            addSlotWidgets(
              new CardSlotWidget(_, Tier.Three),
              new CardSlotWidget(_, Tier.Three),
              new CardSlotWidget(_, Tier.Three),
              new CardSlotWidget(_, Tier.Three),
            )
          }
        }

        cpuSlot = addSlotWidget(new ComputerCpuSlotWidget(_, this, Tier.Three))

        componentBusSlots = addSlotWidgets(
          new ComponentBusSlotWidget(_, Tier.Three),
          new ComponentBusSlotWidget(_, Tier.Three),
          new ComponentBusSlotWidget(_, Tier.Three),
        )

        memorySlots = addSlotWidgets(
          new MemorySlotWidget(_, Tier.Three),
          new MemorySlotWidget(_, Tier.Three),
          new MemorySlotWidget(_, Tier.Three),
          new MemorySlotWidget(_, Tier.Three),
        )

        diskSlots = addSlotWidgets(
          new HddSlotWidget(_, Tier.Three),
          new HddSlotWidget(_, Tier.Three),
          new HddSlotWidget(_, Tier.Three),
          new HddSlotWidget(_, Tier.Three),
        )

        eepromSlot = addSlotWidget(new EepromSlotWidget(_))
    }
  }
}

object ServerItem {
  class Factory(_tier: Tier) extends ItemFactory {
    override type I = ServerItem

    override def itemClass: Class[I] = classOf

    override def name: String = s"Server (${_tier.label})"

    override def tier: Option[Tier] = Some(_tier)

    override def icon: IconSource = IconSource.Items.Server(_tier)

    override def build(): ServerItem = {
      val item = new ServerItem(new Server(_tier))
      item.fillSlotsWithDefaultItems()
      item.syncSlots()
      item
    }

    override def recoverers: Iterable[ItemRecoverer[_, _]] = Some(ItemRecoverer(new ServerItem(_)))
  }
}
