package ocelot.desktop.inventory.item

import ocelot.desktop.graphics.IconSource
import ocelot.desktop.inventory.traits.{CardItem, ComponentItem, PersistableItem}
import ocelot.desktop.inventory.{Item, ItemFactory, ItemRecoverer}
import ocelot.desktop.ui.widget.tooltip.ItemTooltip
import totoro.ocelot.brain.entity.SelfDestructingCard
import totoro.ocelot.brain.entity.traits.{Entity, Environment}
import totoro.ocelot.brain.util.Tier
import totoro.ocelot.brain.util.Tier.Tier

class SelfDestructingCardItem(val card: SelfDestructingCard)
    extends Item with ComponentItem with PersistableItem with CardItem {

  override def entity: Entity with Environment = card

  override def factory: ItemFactory = SelfDestructingCardItem.Factory

  override def fillTooltip(tooltip: ItemTooltip): Unit = {
    super.fillTooltip(tooltip)
    if (card != null) {
      tooltip.addLine(
        if (card.remainingTime < 0) "Fuse has not been set"
        else if (card.remainingTime == 0) "BOOM!"
        else card.remainingTime.toString
      )
    }
  }
}

object SelfDestructingCardItem {
  object Factory extends ItemFactory {
    override type I = SelfDestructingCardItem

    override def itemClass: Class[I] = classOf

    override def name: String = "Self-Destructing Card"

    override def tier: Option[Tier] = Some(Tier.Two)

    override def icon: IconSource = IconSource.Items.SelfDestructingCard

    override def build(): SelfDestructingCardItem = new SelfDestructingCardItem(new SelfDestructingCard)

    override def recoverers: Iterable[ItemRecoverer[_, _]] = Some(ItemRecoverer(new SelfDestructingCardItem(_)))
  }
}
