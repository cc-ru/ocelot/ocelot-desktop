package ocelot.desktop.inventory.item

import ocelot.desktop.graphics.IconSource
import ocelot.desktop.inventory.traits.{ComponentItem, CpuLikeItem, GpuLikeItem, PersistableItem}
import ocelot.desktop.inventory.{Item, ItemFactory, ItemRecoverer}
import totoro.ocelot.brain.entity.APU
import totoro.ocelot.brain.entity.traits.{Entity, GenericCPU, GenericGPU}
import totoro.ocelot.brain.util.Tier.Tier

class ApuItem(val apu: APU) extends Item with ComponentItem with PersistableItem with CpuLikeItem with GpuLikeItem {
  override def entity: Entity with GenericCPU with GenericGPU = apu

  override def factory: ApuItem.Factory = new ApuItem.Factory(apu.tier)
}

object ApuItem {
  class Factory(_tier: Tier) extends ItemFactory {
    override type I = ApuItem

    override def itemClass: Class[I] = classOf

    override def name: String = s"APU (${tier.get.label})"

    // there are T2, T3, and creative APUs
    // however, the APU class starts counting from one (so it's actually T1, T2, and T3)
    // we keep the latter tier internally and increment it when dealing with the rest of the world
    override def tier: Option[Tier] = Some(_tier.saturatingAdd(1))

    override def icon: IconSource = IconSource.Items.Apu(_tier)

    override def build(): ApuItem = new ApuItem(new APU(_tier))

    override def recoverers: Iterable[ItemRecoverer[_, _]] = Some(ItemRecoverer(new ApuItem(_)))
  }
}
