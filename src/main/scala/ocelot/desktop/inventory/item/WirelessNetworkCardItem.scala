package ocelot.desktop.inventory.item

import ocelot.desktop.graphics.IconSource
import ocelot.desktop.inventory.{ItemFactory, ItemRecoverer}
import totoro.ocelot.brain.entity.WirelessNetworkCard
import totoro.ocelot.brain.util.Tier
import totoro.ocelot.brain.util.Tier.Tier

abstract class WirelessNetworkCardItem(override val card: WirelessNetworkCard) extends NetworkCardItem(card) {}

object WirelessNetworkCardItem {
  abstract class Factory extends ItemFactory {
    override def name: String = s"Wireless Net. Card (${tier.get.label})"

    override def icon: IconSource = IconSource.Items.WirelessNetworkCard(tier.get)
  }

  class Tier1(override val card: WirelessNetworkCard.Tier1) extends WirelessNetworkCardItem(card) {
    override def factory: Factory = WirelessNetworkCardItem.Tier1.Factory
  }

  object Tier1 {
    object Factory extends Factory {
      override type I = WirelessNetworkCardItem.Tier1

      override def itemClass: Class[I] = classOf

      override def tier: Option[Tier] = Some(Tier.One)

      override def build(): WirelessNetworkCardItem.Tier1 =
        new WirelessNetworkCardItem.Tier1(new WirelessNetworkCard.Tier1)

      override def recoverers: Iterable[ItemRecoverer[_, _]] = Some(ItemRecoverer(new WirelessNetworkCardItem.Tier1(_)))
    }
  }

  class Tier2(override val card: WirelessNetworkCard.Tier2) extends WirelessNetworkCardItem(card) {
    override def factory: Factory = WirelessNetworkCardItem.Tier2.Factory
  }

  object Tier2 {
    object Factory extends Factory {
      override type I = WirelessNetworkCardItem.Tier2

      override def itemClass: Class[I] = classOf

      override def tier: Option[Tier] = Some(Tier.Two)

      override def build(): WirelessNetworkCardItem.Tier2 =
        new WirelessNetworkCardItem.Tier2(new WirelessNetworkCard.Tier2)

      override def recoverers: Iterable[ItemRecoverer[_, _]] = Some(ItemRecoverer(new WirelessNetworkCardItem.Tier2(_)))
    }
  }
}
