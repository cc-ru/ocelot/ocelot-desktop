package ocelot.desktop.inventory

import ocelot.desktop.graphics.IconSource
import totoro.ocelot.brain.util.Tier.Tier

/** Provides information about a class of [[Item]]s and allows to build an item instance.
  *
  * Used by [[ocelot.desktop.ui.widget.slot.SlotWidget SlotWidgets]]
  * (and [[ocelot.desktop.ui.widget.slot.ItemChooser]]) to tell if you can insert an item into the slot
  * without having to actually construct one.
  * In particular, make sure the [[tier]] and [[itemClass]] provided by the factory are accurate.
  */
trait ItemFactory {

  /** The concrete type of the [[Item]] built by the factory. */
  type I <: Item

  /** The runtime class of the [[Item]] built by the factory.
    *
    * @note It's expected that `build().getClass == itemClass`.
    */
  def itemClass: Class[I]

  /** A name that represents what will be built by the factory.
    *
    * Usually [[name]] and [[Item.name]] are the same (in fact, the latter defaults to this unless overridden).
    *
    * Used by the [[ocelot.desktop.ui.widget.slot.ItemChooser ItemChooser]] in its entries.
    */
  def name: String

  /** The tier of an item this factory will construct in its [[build]] method.
    *
    * @note It's expected that `build().tier == tier`.
    */
  def tier: Option[Tier]

  /** The icon of an item this factory will construct in its [[build]] method.
    *
    * Used by the [[ocelot.desktop.ui.widget.slot.ItemChooser ItemChooser]] in its entries.
    */
  def icon: IconSource

  /** Constructs a new item instance. */
  def build(): I

  /** Returns a list of recoverers provided by the factory, */
  def recoverers: Iterable[ItemRecoverer[_, _]]
}
