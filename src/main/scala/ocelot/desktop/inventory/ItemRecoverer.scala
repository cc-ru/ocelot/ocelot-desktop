package ocelot.desktop.inventory

import scala.reflect.{classTag, ClassTag}

/** Allows recovering an [[Item]] from [[S]] (typically an [[totoro.ocelot.brain.entity.traits.Entity Entity]]).
  *
  * This is used for migration from old saves before the inventory system was introduced to Ocelot Desktop.
  */
final class ItemRecoverer[S: ClassTag, I <: Item](f: S => I) {
  val sourceClass: Class[_] = classTag[S].runtimeClass

  def recover(source: S): I = f(source)
}

object ItemRecoverer {
  def apply[S: ClassTag, I <: Item](f: S => I): ItemRecoverer[S, I] = new ItemRecoverer(f)
}
