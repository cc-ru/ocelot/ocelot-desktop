package ocelot.desktop.inventory.traits

import ocelot.desktop.graphics.IconSource
import ocelot.desktop.inventory.Item
import ocelot.desktop.ui.UiHandler
import ocelot.desktop.ui.widget.contextmenu.{ContextMenu, ContextMenuEntry}
import ocelot.desktop.ui.widget.tooltip.ItemTooltip
import totoro.ocelot.brain.entity.traits.{Entity, Environment}

/** Implemented by [[Item]]s that wrap a component. */
trait ComponentItem extends EntityItem {
  override def entity: Entity with Environment

  override def shouldReceiveEventsFor(address: String): Boolean =
    super.shouldReceiveEventsFor(address) || Option(entity.node).exists(_.address == address)

  override def fillTooltip(tooltip: ItemTooltip): Unit = {
    super.fillTooltip(tooltip)

    if (showAddress && entity.node.address != null) {
      tooltip.addLine(s"Address: ${entity.node.address}")
    }
  }

  private val copyAddressEntry = ContextMenuEntry("Copy address", IconSource.Copy) {
    UiHandler.clipboard = entity.node.address
  }

  override def fillRmbMenu(menu: ContextMenu): Unit = {
    menu.addEntry(copyAddressEntry)

    super.fillRmbMenu(menu)
  }

  override def update(): Unit = {
    super.update()

    copyAddressEntry.setEnabled(entity.node.address != null)
  }

  def showAddress: Boolean = true
}
