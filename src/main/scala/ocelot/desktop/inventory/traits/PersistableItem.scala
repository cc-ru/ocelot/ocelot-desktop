package ocelot.desktop.inventory.traits

import ocelot.desktop.inventory.Item
import ocelot.desktop.util.Persistable

/** Provides serialization and deserialization for an [[Item]]. */
trait PersistableItem extends Item with Persistable
