package ocelot.desktop.inventory.traits

import ocelot.desktop.inventory.Item
import ocelot.desktop.node.nodes.RackNode
import ocelot.desktop.ui.widget.contextmenu.ContextMenu
import totoro.ocelot.brain.entity.result
import totoro.ocelot.brain.entity.traits.{Entity, RackMountable}

trait RackMountableItem extends Item with ComponentItem {
  override def entity: Entity with RackMountable

  def isInRack: Boolean = slot.exists(_.inventory.isInstanceOf[RackNode])

  override def fillRmbMenu(menu: ContextMenu): Unit = {
    RackNode.addContextMenuEntriesOfMountable(menu, Some(this))

    super.fillRmbMenu(menu)
  }
}
