package ocelot.desktop.inventory.traits

import ocelot.desktop.inventory.Item

/** A marker trait implemented by [[Item]]s that can be put into card slots. */
trait CardItem extends ComponentItem
