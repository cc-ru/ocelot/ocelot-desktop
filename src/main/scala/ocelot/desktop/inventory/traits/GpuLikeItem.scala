package ocelot.desktop.inventory.traits

import ocelot.desktop.ui.widget.tooltip.ItemTooltip
import totoro.ocelot.brain.Settings
import totoro.ocelot.brain.entity.traits.{Entity, GenericGPU}
import totoro.ocelot.brain.util.ColorDepth

trait GpuLikeItem extends ComponentItem {
  override def entity: Entity with GenericGPU

  override def fillTooltip(tooltip: ItemTooltip): Unit = {
    super.fillTooltip(tooltip)
    if (entity != null) {
      val resolution = Settings.screenResolutionsByTier(entity.tier.id)
      tooltip.addLine(s"Max resolution: ${resolution._1}×${resolution._2}")
      val depth = Settings.screenDepthsByTier(entity.tier.id) match {
        case ColorDepth.OneBit => "1 bit"
        case ColorDepth.FourBit => "4 bit"
        case ColorDepth.EightBit => "8 bit"
      }
      tooltip.addLine(s"Max color depth: $depth")
      tooltip.addLine(s"VRAM: ${entity.totalVRAM.toInt}")
    }
  }
}
