package ocelot.desktop.inventory.traits

import ocelot.desktop.graphics.IconSource
import ocelot.desktop.inventory.Item
import ocelot.desktop.inventory.traits.CpuLikeItem.CpuArchitectureChangedNotification
import ocelot.desktop.ui.widget.contextmenu.{ContextMenu, ContextMenuEntry, ContextMenuIcon, ContextMenuSubmenu}
import ocelot.desktop.ui.widget.tooltip.ItemTooltip
import totoro.ocelot.brain.entity.machine.MachineAPI
import totoro.ocelot.brain.entity.traits.{Entity, GenericCPU}

/** An [[Item]] that acts as a CPU and can therefore be inserted into a CPU slot. */
trait CpuLikeItem extends ComponentItem {
  override def entity: Entity with GenericCPU

  override def fillRmbMenu(menu: ContextMenu): Unit = {
    menu.addEntry(new ContextMenuSubmenu("Set architecture", Some(ContextMenuIcon(IconSource.Microchip))) {
      for (arch <- entity.allArchitectures) {
        val name = MachineAPI.getArchitectureName(arch) +
          (if (arch == entity.architecture) " (current)" else "")
        val entry = ContextMenuEntry(name) {
          entity.setArchitecture(arch)
          notifySlot(CpuArchitectureChangedNotification)
        }

        entry.setEnabled(arch != entity.architecture)
        addEntry(entry)
      }
    })

    super.fillRmbMenu(menu)
  }

  override def fillTooltip(tooltip: ItemTooltip): Unit = {
    super.fillTooltip(tooltip)
    if (entity != null) {
      tooltip.addLine(s"Components: ${entity.supportedComponents}")
    }
  }
}

object CpuLikeItem {
  case object CpuArchitectureChangedNotification extends Item.Notification
}
