package ocelot.desktop.windows

import ocelot.desktop.inventory.Item
import ocelot.desktop.inventory.traits.PersistableItem
import ocelot.desktop.node.nodes.ChestNode
import ocelot.desktop.ui.layout.{Layout, LinearLayout}
import ocelot.desktop.ui.widget.Widget
import ocelot.desktop.ui.widget.slot.SlotWidget
import ocelot.desktop.ui.widget.window.PanelWindow
import ocelot.desktop.util.Orientation
import ocelot.desktop.windows.ChestWindow.{Columns, Rows}

import scala.collection.mutable.ArrayBuffer

class ChestWindow(node: ChestNode) extends PanelWindow {
  private val slots = ArrayBuffer.empty[SlotWidget[_]]

  setInner(new Widget {
    override protected val layout: Layout = new LinearLayout(this, orientation = Orientation.Vertical)

    for (row <- 0 until Rows) {
      children :+= new Widget {
        override protected val layout: Layout = new LinearLayout(this, orientation = Orientation.Horizontal)

        for (column <- 0 until Columns) {
          val index = row * Columns + column
          val slot = new SlotWidget[Item with PersistableItem](node.Slot(index))
          children :+= slot
          slots += slot
        }
      }
    }
  })

  override protected def title: String = "Chest " + node.label.getOrElse("")

  override def titleMaxLength: Int = 36

  override def dispose(): Unit = {
    super.dispose()
    for (slot <- slots) {
      slot.dispose()
    }
  }
}

object ChestWindow {
  val Rows = 8
  val Columns = 9
}
