package ocelot.desktop.windows

import ocelot.desktop.geometry.Padding2D
import ocelot.desktop.node.nodes.DiskDriveNode
import ocelot.desktop.ui.widget.PaddingBox
import ocelot.desktop.ui.widget.window.PanelWindow
import ocelot.desktop.util.DiskDriveAware

class DiskDriveWindow(diskDriveHost: DiskDriveAware) extends PanelWindow {
  setInner(new PaddingBox(diskDriveHost.floppySlotWidget, Padding2D(8, 64, 8, 64)))

  override protected def title: String = "Disk Drive " + (diskDriveHost match {
    case diskDriveNode: DiskDriveNode => diskDriveNode.label
    case _ => None
  }).getOrElse("")

  override def titleMaxLength: Int = 16
}
