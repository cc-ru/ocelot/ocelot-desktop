package ocelot.desktop.windows

import ocelot.desktop.ColorScheme
import ocelot.desktop.color.Color
import ocelot.desktop.geometry.{Padding2D, Size2D}
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.node.nodes.RaidNode
import ocelot.desktop.ui.layout.{AlignItems, Layout, LinearLayout}
import ocelot.desktop.ui.widget._
import ocelot.desktop.ui.widget.slot.HddSlotWidget
import ocelot.desktop.ui.widget.window.PanelWindow
import ocelot.desktop.util.Orientation
import totoro.ocelot.brain.util.Tier

class RaidWindow(raidNode: RaidNode) extends PanelWindow {
  override protected def title: String = s"Raid ${raidNode.raid.filesystem.map(_.node.address).getOrElse("")}"

  override def minimumSize: Size2D = Size2D(350, 147)
  override def maximumSize: Size2D = minimumSize

  private val hddSlotWidgets: Array[HddSlotWidget] = new Array[HddSlotWidget](raidNode.raid.getSizeInventory)

  setInner(
    new Widget {
      override protected val layout: Layout = new LinearLayout(
        this,
        orientation = Orientation.Vertical,
        gap = 10,
        alignItems = AlignItems.Start,
      )

      // HDD slots & background border
      children :+= new PaddingBox(
        new Widget {
          override def minimumSize: Size2D = Size2D(132, 52)
          override def maximumSize: Size2D = minimumSize

          override def draw(g: Graphics): Unit = {
            // Background border
            g.sprite(
              "window/raid/Slots",
              position.x,
              position.y,
              size.width,
              size.height,
            )

            super.draw(g)
          }

          // Slots
          children :+= new PaddingBox(
            new Widget {
              override protected val layout: Layout = new LinearLayout(
                this,
                orientation = Orientation.Horizontal,
                gap = 4,
              )

              var hddSlotWidget: HddSlotWidget = _

              for (i <- hddSlotWidgets.indices) {
                hddSlotWidget = new HddSlotWidget(raidNode.Slot(i), Tier.Three)
                hddSlotWidgets(i) = hddSlotWidget
                children :+= hddSlotWidget
              }
            },
            Padding2D(left = 8, top = 8),
          )
        },
        Padding2D(left = 94, top = 10),
      )

      // Text under HDDs
      children :+= new PaddingBox(
        new Widget {
          override protected val layout: Layout = new LinearLayout(
            this,
            orientation = Orientation.Vertical,
            gap = 2,
          )

          def addLabel(data: String): Unit = {
            children :+= new Label {
              override def text: String = data
              override def color: Color = ColorScheme("LabelError")
            }
          }

          addLabel("Adding a disk wipes it.")
          addLabel("Removing a disk wipes the raid.")
        },
        Padding2D(left = 5),
      )
    }
  )
}
