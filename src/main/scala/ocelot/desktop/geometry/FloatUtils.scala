package ocelot.desktop.geometry

object FloatUtils {
  implicit class ExtendedFloat(val v: Float) extends AnyVal {
    def lerp(that: Float, alpha: Float): Float = v * (1 - alpha) + that * alpha

    def clamp(min: Float = 0f, max: Float = 1f): Float = v.min(max).max(min)
  }
}
