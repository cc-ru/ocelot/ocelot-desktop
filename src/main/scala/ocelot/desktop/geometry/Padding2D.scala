package ocelot.desktop.geometry

object Padding2D {
  def equal(v: Float): Padding2D = Padding2D(v, v, v, v)
}

//noinspection ScalaWeakerAccess
case class Padding2D(top: Float = 0, right: Float = 0, bottom: Float = 0, left: Float = 0) {
  def horizontal: Float = left + right

  def vertical: Float = top + bottom

  def sizeAddition: Size2D = Size2D(horizontal, vertical)

  def offset: Vector2D = Vector2D(left, top)
}
