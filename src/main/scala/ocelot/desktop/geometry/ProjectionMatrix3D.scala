package ocelot.desktop.geometry

object ProjectionMatrix3D {
  def perspective(aspect: Float, fovY: Float, zNear: Float, zFar: Float): ProjectionMatrix3D = {
    val f = (1.0 / math.tan(math.toRadians(fovY) / 2.0)).toFloat
    // format: off
    ProjectionMatrix3D(
      f / aspect, 0, 0, 0,
      0, f, 0, 0,
      0, 0, (zFar + zNear) / (zNear - zFar), (2 * zFar * zNear) / (zNear - zFar),
      0, 0, -1, 0,
    )
  }
}

case class ProjectionMatrix3D(m11: Float, m12: Float, m13: Float, m14: Float,
                              m21: Float, m22: Float, m23: Float, m24: Float,
                              m31: Float, m32: Float, m33: Float, m34: Float,
                              m41: Float, m42: Float, m43: Float, m44: Float) {
  def array: Array[Float] = Array(m11, m12, m13, m14, m21, m22, m23, m24, m31, m32, m33, m34, m41, m42, m43, m44)
}
