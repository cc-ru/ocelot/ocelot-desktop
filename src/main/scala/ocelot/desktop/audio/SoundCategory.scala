package ocelot.desktop.audio

import ocelot.desktop.Settings

//noinspection ScalaWeakerAccess
object SoundCategory extends Enumeration {
  val Environment, Beep, Interface, Records = Value

  def getSettingsValue(soundCategory: SoundCategory.Value): Float = soundCategory match {
    case Environment => Settings.get.volumeEnvironment
    case Records => Settings.get.volumeRecords
    case Beep => Settings.get.volumeBeep
    case Interface => Settings.get.volumeInterface
  }
}
