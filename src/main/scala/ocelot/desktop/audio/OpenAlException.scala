package ocelot.desktop.audio

import scala.util.control
import scala.util.control.Exception.Catch

case class OpenAlException(func: String, errName: String, code: Int) extends Exception(s"OpenAL error: $func: $errName")

object OpenAlException {
  def defaulting[T](default: => T): Catch[T] = control.Exception.failAsValue(classOf[OpenAlException])(default)

  def ignoring: Catch[Unit] = defaulting(())
}
