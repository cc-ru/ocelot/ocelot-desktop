package ocelot.desktop.audio

trait SoundStream {
  def enqueue(samples: SoundSamples): Unit
}
