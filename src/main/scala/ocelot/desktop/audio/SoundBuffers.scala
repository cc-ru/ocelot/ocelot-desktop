package ocelot.desktop.audio

import ocelot.desktop.util.Resource

import scala.collection.mutable.ArrayBuffer

object SoundBuffers extends Resource {
  lazy val MachineComputerRunning: SoundBuffer = load("/ocelot/desktop/sounds/machine/computer_running.ogg")

  lazy val MachineFloppyAccess: Array[SoundBuffer] = Array(
    load("/ocelot/desktop/sounds/machine/floppy_access1.ogg"),
    load("/ocelot/desktop/sounds/machine/floppy_access2.ogg"),
    load("/ocelot/desktop/sounds/machine/floppy_access3.ogg"),
    load("/ocelot/desktop/sounds/machine/floppy_access4.ogg"),
    load("/ocelot/desktop/sounds/machine/floppy_access5.ogg"),
    load("/ocelot/desktop/sounds/machine/floppy_access6.ogg"),
  )

  lazy val MachineFloppyEject: SoundBuffer = load("/ocelot/desktop/sounds/machine/floppy_eject.ogg")
  lazy val MachineFloppyInsert: SoundBuffer = load("/ocelot/desktop/sounds/machine/floppy_insert.ogg")

  lazy val MachineHDDAccess: Array[SoundBuffer] = Array(
    load("/ocelot/desktop/sounds/machine/hdd_access1.ogg"),
    load("/ocelot/desktop/sounds/machine/hdd_access2.ogg"),
    load("/ocelot/desktop/sounds/machine/hdd_access3.ogg"),
    load("/ocelot/desktop/sounds/machine/hdd_access4.ogg"),
    load("/ocelot/desktop/sounds/machine/hdd_access5.ogg"),
    load("/ocelot/desktop/sounds/machine/hdd_access6.ogg"),
  )

  lazy val MachineTapeButtonPress: SoundBuffer = load("/ocelot/desktop/sounds/machine/tape_button_press.ogg")
  lazy val MachineTapeButtonRelease: SoundBuffer = load("/ocelot/desktop/sounds/machine/tape_button_release.ogg")
  lazy val MachineTapeEject: SoundBuffer = load("/ocelot/desktop/sounds/machine/tape_eject.ogg")
  lazy val MachineTapeInsert: SoundBuffer = load("/ocelot/desktop/sounds/machine/tape_insert.ogg")
  lazy val MachineTapeRewind: SoundBuffer = load("/ocelot/desktop/sounds/machine/tape_rewind.ogg")

  lazy val InterfaceClickPress: SoundBuffer = load("/ocelot/desktop/sounds/interface/click_press.ogg")
  lazy val InterfaceClickRelease: SoundBuffer = load("/ocelot/desktop/sounds/interface/click_release.ogg")
  lazy val InterfaceTickPress: SoundBuffer = load("/ocelot/desktop/sounds/interface/tick_press.ogg")
  lazy val InterfaceTickRelease: SoundBuffer = load("/ocelot/desktop/sounds/interface/tick_release.ogg")
  lazy val InterfaceShutter: SoundBuffer = load("/ocelot/desktop/sounds/interface/shutter.ogg")

  lazy val MinecraftClickPress: SoundBuffer = load("/ocelot/desktop/sounds/minecraft/click_press.ogg")
  lazy val MinecraftClickRelease: SoundBuffer = load("/ocelot/desktop/sounds/minecraft/click_release.ogg")
  lazy val MinecraftExplosion: SoundBuffer = load("/ocelot/desktop/sounds/minecraft/explosion.ogg")

  lazy val NoteBlock: Map[String, SoundBuffer] = List(
    "banjo", "basedrum", "bass", "bell", "bit", "chime", "cow_bell", "didgeridoo", "flute", "guitar",
    "harp", "hat", "iron_xylophone", "pling", "snare", "xylophone",
  ).map(name => {
    (name, load(s"/ocelot/desktop/sounds/minecraft/note_block/$name.ogg"))
  }).toMap

  private val loaded = new ArrayBuffer[SoundBuffer]()

  private def load(file: String): SoundBuffer = {
    val buffer = new SoundBuffer(file)
    loaded.append(buffer)
    buffer
  }

  override def freeResource(): Unit = {
    super.freeResource()
    loaded.foreach(_.freeResource())
  }
}
