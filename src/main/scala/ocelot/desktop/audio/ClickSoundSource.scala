package ocelot.desktop.audio

trait ClickSoundSource {
  def press: SoundSource
  def release: SoundSource
}
