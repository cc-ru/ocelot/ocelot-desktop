package ocelot.desktop.geometry

import ocelot.desktop.util.UnitTest

class Vector2DTest extends UnitTest {
  test("Adding two vectors") {
    val a = Vector2D(1.0, 2.0)
    val b = Vector2D(3.0, -1.0)
    assert(a + b == Vector2D(4.0, 1.0))
  }
}
