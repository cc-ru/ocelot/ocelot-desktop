# Contributing to Ocelot
The development of Ocelot is a volunteer effort — and that means we're always understaffed.
If you'd like to help us out, we'll appreciate it greatly!
And to help *you* help us, we've written a guide, which you are reading right now, describing how we do things around here.

## The Ocelot projects
In fact, Ocelot consists of multiple projects, of which Ocelot Desktop is but a part.

The most important component is [ocelot-brain].
It is a Scala library that does all the actual OC emulation.
In terms of responsibilities, it roughly corresponds to the server-side OpenComputers code except abstracted away from Minecraft specifics.
It's ocelot-brain that defines the behavior of all entities: blocks, cards, etc.; and it's also ocelot-brain that runs all the Lua code.

Ocelot Desktop builds on top of ocelot-brain and provides a graphical interface to the library.
It does rendering, audio, handles user interaction, and translates user input into calls to ocelot-brain.
This is also where all items are defined.

Such architectural separation of concerns admits alternative interfaces to ocelot-brain.
[Ocelot.online] is a demonstration of that — it uses the same ocelot-brain library for emulation but provides a web interface instead.
At the moment Ocelot.online is not particularly full of features, unfortunately, and we won't focus on it in this document.

## Issues
We use GitLab issues to track feature requests and bugs.
As touched on in the previous section, we have three repositories — and, therefore, three issue trackers.
Which one should you report your issue to?

- If you want to request support for a new OC component or report incorrect emulation of an existing component, consider posting an issue to the [ocelot-brain] tracker.
- If you'd like to tell us about UI glitches (or ask for some UI improvement), this is squarely in the Ocelot Desktop territory.
- Since [Ocelot.online] is not actively developed, it's unlikely that it would be a good choice for an issue — we'd prefer if you told us about issues directly via IRC, Discord, or email.

If you're not quite sure which one to pick anyway, Ocelot Desktop is a safe bet.
Don't worry if you make a mistake — we can always move the issue to the appropriate place.

### Feature requests
Ocelot Desktop wouldn't become so great if nobody was asking for features!
However, if you'd like to ask for one, keep in mind that we are a small group of developer volunteering our time and attention to this project.
Many issues stayed open for several years before somebody went ahead and implemented the requested features.

So we'll appreciate it if you tell us why you're requesting a feature and how you would use it if it were added.
Seeing a clear use-case helps us stay on track if we get to implementing it — and it's also a great source of motivation for developers since this shows how, concretely, their efforts can help you. :)

### Bugs
Bugs may be embarrassing — but this is no reason to hide them.
On the contrary, we'd love to hear about them.
If you find one, **please tell us** — or, better yet, **report it on the issue tracker**, which makes sure we don't accidentally forget about it and also helps other users who have the same problem!

Ocelot Desktop should never crash.
This is, of course, but a hopeless dream, and it does crash sometimes.
**Please do not ignore crashes** (unless you're sure Ocelot is not at fault): we realize this is the most disruptive kind of bug, and we'd like to fix it as soon as possible.

If you're reporting a bug, please **describe your actions** preceding it: was it pressing a button? loading a workspace? or something else?
Ideally (if you can reproduce the issue — that is, trigger it again), you should tell us step-by-step what you did that led to the problem.
But we understand that it's simply impossible sometimes — still, we'd rather have a report alerting us of the bug than be completely oblivious to it.

Please attach logs if you have them (`%APPDATA%/Ocelot/Logs/Desktop.log` on Windows, `~/.local/share/ocelot/desktop.log` on Linux) — and **especially** if it's a crash.
Logs don't always help us, but when they do, they help tremendously: a crash would have a stack trace pointing to the exact line of code that triggered it, for example.

If the bug only happens with a specific workspace, you could attach the save directory (as a zip archive) as well.

Finally, sometimes we'd like to ask for additional information after we get a bug report.
Though we don't expect an instant reply, it would be sad if you just ghosted us.
We don't have a stale issue policy, but we may close issues that were reported such a long time ago that they are likely no longer relevant.

## Code contribution
Admittedly, there's little documentation, and Scala, a nice language though it might be, is not among the most popular choices for software development.
Just to warn you of what kind of mess you're getting yourself into if you're going to contribute code. :)
This means you'll probably have to figure out how things work by reading the source code.

It would be a good idea to chat with us if you're not deterred because of the admonition above.
Especially if you're implementing a new feature.
Doing this can help you avoid some pitfalls and get a better idea of how things should work.

New code should be submitted as a merge request.
We'll review the changes, perhaps suggesting some things to improve.
Once we're happy with the code, we'll gladly accept the MR.

If your work requires changing ocelot-brain and Ocelot Desktop at the same time, you'll need to make two MRs — one for each repository.

Here are a few things we'd like you to do when writing code.

- Keep sensible commit names unless you're planning to squash them at the end.

- Do not commit code that doesn't compile.
  It makes bisection harder.

- Try to adhere to the style of existing code.
  It might be a bit haphazard currently, but please avoid introducing yet another coding convention.

  - Speaking of conventions: we use Scala's naming scheme.
    For instance, immutable constants are spelled `LikeThis`.

- Use Scala idioms.
  The language features an extensive standard library that covers many aspects of programming.
  Using `Option[T]` instead of `null`, modeling data as case classes extending a sealed trait, and using pattern matching — these things, though perhaps not as widely used in mainstream languages like C++ or Java, make code safer and more consistent.

## Communication
The primary means of communication is via [IRC] or [Discord] (thread "forums ‣ Ocelot Dev").
Either English or Russian is fine.
(Please give people some time to respond — they might even be asleep at the time you're posting a message.)

If you despise instant messaging, you can shoot us an email to no-reply at fomalhaut me.
(Don't mind the weird address.)

[ocelot-brain]: https://gitlab.com/cc-ru/ocelot/ocelot-brain "The ocelot-brain repository."
[Ocelot.online]: https://gitlab.com/cc-ru/ocelot/ocelot-online "The Ocelot.online repository."
[IRC]: ircs://irc.esper.net:6697/cc.ru "#cc.ru at irc.esper.net"
[Discord]: https://discord.com/invite/FM9qWGm
