# Ocelot Desktop
![Banner][banner]

A desktop version of the renowned OpenComputers emulator Ocelot.

[**Download** the latest stable release][download-stable]
or [the latest development build][download-dev] ([mirror][download-dev-mirror]).

## Why
You might already be happy with your choice of an OC emulator;
there is a plenty of them, after all.
Why would you want to reconsider your life choices now all of a sudden?
A fine question, indeed; perhaps, a list of features will persuade you.

### Powered by ocelot-brain
At the heart of this emulator is [ocelot-brain][ocelot-brain] (uh, don't ask me),
which is essentially the source code of OpenComputers decoupled of everything
Minecraft-specific and repurposed as a Scala library.
This makes Ocelot Desktop **the most accurate emulator** ever made.
Your programs will run on the Lua implementation used by the mod and exhibit
similar timings.
The performance and memory constraints present in OC are also emulated.

### Customizable setups
Taylor your computer build to your needs!
We provide a variety of components to choose from:

- graphics cards
- network cards (wired, wireless)
- linked cards
- internet cards
- redstone cards (including the second tier!)
- data cards
- hard disks
- floppy disks (in T3+ computer cases only, just like in OpenComputers)

Feel limited by the vanilla cards?
No problem — we've even integrated some components from popular addons!

![Addon showcase][addon-showcase]

- **Computronics:**
  - `computer.beep()` does not excite your music sense enough?
    Check out the **sound card**'s synthesis engine,
    or string notes together on a bunch of **note blocks**!

  - You have privacy concerns?
    An ultra-precision blast of the **self-destructing card** might save your day!

  - If, on the contrary, you're the kind to violate the privacy of others,
    we've got a **camera** that reads video data from a real webcam.

  - Those who seem to find no color in their life may appreciate the
    **colorful lamps**.
    Their iridescent glow will provide comfort in the darkest hour.
    And they won't desert you.

- **OpenFM:** for those lo-fi beats to chill and relax to while writing code.

We'll make sure your setups are grounded in reality
by having the maximum card tier in a slot depend on your computer case.
Oh, did I forget to mention Ocelot Desktop has APUs as well?

Explore the wonders of distributed computing by adding a couple of extra
computers to your workspace.
(Or a thousand — if you think your host can handle this.)
Network cards allow these newly spawned machines to talk to each other.
And relays may prove useful to manage wired networks.

Perhaps, instead of enlisting a computer army,
you want to attach a dozen of screens to a single machine
(as they do in the movies).
Well, no problem — we've got that covered, too.

When a plain computer will not do, a server will.
Build your own scalable cloud infrastructure at no cost!
We'll supply you the hardware to meet the demand: racks, servers, and rack disk
drives.

### Gorgeous graphical interface
![GUI][gui]

Manage your screen real estate to avoid distractions.
All nodes are draggable, as are windows.
Screen windows in particular are also resizeable —
click and drag the bottom-right corner if they take up too much space.
Or hold <kbd>Shift</kbd> and let the window consume it all.

![Window scaling][window-scaling]

Many additional options are hidden in the context menus —
try hitting the right mouse button on various things.
For example, components will let you copy their address to the clipboard,
and right-clicking on the TPS counter (on the bottom right)
allows you to change the simulation speed.

![TPS rate menu][tps-menu]

Ocelot Desktop uses hardware acceleration to offload the daunting task of
rendering its interface to a special-purposed device (your GPU),
so make sure you have an **OpenGL 3.2-capable** graphics card.
(Though, honestly, it's harder to find one that isn't, really.)

### Persistable workspaces
Imagine putting many hours into wiring things up only to have to do it all from
scratch the next time you open the emulator.
That... would be sad and disappointing.
I mean, OpenComputers can persist its machines just fine, right?
Good news: by reusing its code, we've essentially inherited the ability
to save workspaces on the disk and load them afterward!

Just in case, Ocelot Desktop will warn you if you smash the quit button without
saving.
We'd rather you didn't feel sad and disappointed.

### Cool features
![Performance graphs][perf-graphs]
![Sound card GUI][sound-card]

A few smaller features are worth mentioning, too:

- Windows show the corresponding block's address by default.
  However, you can relabel them: look for the option in the context menu.
- The button ![][drawer-button] at the bottom of a computer case window
  shows performance graphs: the memory, processor time, and call budget.
- Hold the <kbd>Ctrl</kbd> key while dragging blocks to snap them to the grid.

Full list of hotkeys and supported features is available in the [FAQ][ocelot-desktop-faq].

## Download
Decided to give Ocelot Desktop a shot?

[**Download** the latest stable release][download-stable]
or [the latest development build][download-dev] ([mirror][download-dev-mirror]).

## Hacking
Just import the project in your favorite IDE.  
Make sure to have Scala and SBT installed (your IDE may assist you with that).

We include [ocelot-brain][] as a Git submodule: don't forget to fetch it!

```sh
$ git submodule update --init --recursive
```

To build from source and start Ocelot Desktop, type:

```sh
$ sbt run
```

If you want to get a JAR, use this:

```sh
$ sbt assembly
```

(You'll find it at `target/scala-2.13/ocelot-desktop.jar`.)

In case you see the compiler complain about `BuildInfo` or just want to refresh
the version displayed in the window title and the logs, run:

```sh
$ sbt buildInfo
```

If you need to add additional sprites or modify existing ones, do it in the
`sprites` directory. When everything is ready, you need to pack them into a
texture atlas by running the `SpritePack` tool located in the project's
home directory.

## Credits
- **LeshaInc:** the author of Ocelot Desktop and the graphics guru.
- **Totoro:** the creator of [ocelot-brain][] and [ocelot.online][ocelot-online].
- **bpm140:** produced the marvelous Ocelot Desktop [landing page][ocelot-desktop].
- **rason:** stirred development at the critical moment!
- **NE0:** the bug extermination specialist.
- **ECS:** introduced the exciting realm of cloud computing.
- **Smok1e:** added some light and color with new components from Computronics.
- **Saphire:** scaled the UI for HiDPI screens.
- **fingercomp:** wrote this README.

## See also
- The [Ocelot Desktop][ocelot-desktop] web page
  (links to the latest build, FAQ).
- [ocelot.online][ocelot-online], a rudimentary web version of Ocelot.
- [ocelot-brain][], the backend library of Ocelot Desktop.
- [#cc.ru on IRC][irc] if you have any questions
  (Russian, but we're fine with English too).
- Or [Discord][discord] if that's your fancy (we won't judge).

[banner]: ./assets/banner.png "The Ocelot banner"
[download-stable]: https://gitlab.com/cc-ru/ocelot/ocelot-desktop/-/releases/permalink/latest
[download-dev]: https://cc-ru.gitlab.io/ocelot/ocelot-desktop/ocelot.jar
[download-dev-mirror]: https://ocelot.fomalhaut.me/ocelot.jar
[addon-showcase]: ./assets/addon-showcase.png "A workspace with colorful lamps and an OpenFM radio."
[gui]: ./assets/gui.gif "A screenshot of the GUI."
[window-scaling]: ./assets/window-scale.gif "Demonstrates the screen scaling."
[tps-menu]: ./assets/tps.png "The simulation speed menu (right-click on the TPS)."
[perf-graphs]: ./assets/perf-graphs.gif "A demo of performance graphs."
[sound-card]: ./assets/sound-card.gif "Shows the sound card UI while playing a melody."
[drawer-button]: ./sprites/buttons/BottomDrawerOpen.png
[ocelot-brain]: https://gitlab.com/cc-ru/ocelot/ocelot-brain
[ocelot-online]: https://ocelot.fomalhaut.me/
[ocelot-desktop]: https://ocelot.fomalhaut.me/desktop/
[ocelot-desktop-faq]: https://ocelot.fomalhaut.me/faq.html
[irc]: ircs://irc.esper.net:6697/cc.ru
[discord]: https://discord.com/invite/FM9qWGm
